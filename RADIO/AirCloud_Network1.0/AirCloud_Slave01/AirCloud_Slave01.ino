    #include <RF24Network.h>
    #include <RF24.h>
    #include <SPI.h>
  
    RF24 radio(7, 8);               // nRF24L01 (CE,CSN)
    RF24Network network(radio);      // Include the radio in the network
    const uint16_t this_node = 01;   // Address of our node in Octal format ( 04,031, etc)
    const uint16_t master00 = 00;    // Address of the other node in Octal format


    void setup()
    {
      SPI.begin();
      radio.begin();
      network.begin(90, this_node); //(channel, node address)
      radio.setDataRate(RF24_2MBPS); //oppure setDataRate(RF24_250KBPS); quale conviene?
      Serial.begin(9600);
    }
    
    void loop()
    {
      network.update();
      unsigned long got_time;

      if(network.available())
      {         
            while(network.available())
            {
              Serial.print(F("While, "));
              RF24NetworkHeader header;
              network.read( header, &got_time, sizeof(unsigned long) );
            }
            
            Serial.print(got_time);
            
            unsigned long feedback_time = got_time;
            Serial.print(F(", Mi preparo per inviare, "));
            RF24NetworkHeader header00(00);
            if(network.write(header00, &feedback_time, sizeof(feedback_time)))  // Send the data
            { 
              Serial.print(F("Invio risposta "));
              Serial.println(feedback_time);
            }
            else
            {
              Serial.println(F("Invio fallito"));
            }
      }
   }


  /*
   * COME SCRIVERE
   * unsigned long buttonState = digitalRead(button);
   * RF24NetworkHeader header4(node012);                                      // (Address where the data is going)
   * bool ok3 = network.write(header4, &buttonState, sizeof(buttonState));    // Send the data
   *
   *COME RICEVERE
   *RF24NetworkHeader header;
   *unsigned long incomingData;
   *network.read(header, &incomingData, sizeof(incomingData)); // Read the incoming data
   */



      
   
