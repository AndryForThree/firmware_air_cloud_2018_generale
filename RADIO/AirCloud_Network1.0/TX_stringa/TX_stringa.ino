    #include <RF24Network.h>
    #include <RF24.h>
    #include <SPI.h>
  
    RF24 radio(7, 8);               // nRF24L01 (CE,CSN)
    RF24Network network(radio);      // Include the radio in the network
    const uint16_t this_node = 01;   // Address of our node in Octal format ( 04,031, etc)
    const uint16_t master00 = 00;    // Address of the other node in Octal format

    unsigned long interval = 4000;
    unsigned long last_sent = 0;


    void setup()
    {
      SPI.begin();
      radio.begin();
      network.begin(90, this_node); //(channel, node address)
      radio.setDataRate(RF24_250KBPS); //oppure setDataRate(RF24_250KBPS); quale conviene?
      Serial.begin(9600);
    }
    
    void loop()
    {
      // Pump the network regularly
      network.update();

      // If it's time to send a message, send it!
      unsigned long now = millis();
      if ( now - last_sent >= interval )
      {
       last_sent = now;

       Serial.print("Sending...");
       int len = 6;
       char sendData[6] = "Andrea";
       RF24NetworkHeader header(00);
       //bool ok = network.write(header,&sendData,strlen(sendData));
       bool ok = network.write(header,&sendData,len);
       Serial.print("Sending - ");
       Serial.print(sendData);
       Serial.print(" - ");
       Serial.print(strlen(sendData));  
       Serial.print(" bytes long. ");
       Serial.println();
       
       if (ok)
       {
        Serial.println("ok.");
       }
       else
       {
        Serial.println("failed.");
       }
       
       //delay(500);
        
       /*Serial.print("Sending...");
       char sendData1[6]="Matteox";
       //RF24NetworkHeader header(00);
       bool ok1 = network.write(header,sendData1,strlen(sendData1));
       Serial.print(strlen(sendData1));*/
      } 
     delay(2000);    
    }


  /*
   * COME SCRIVERE
   * unsigned long buttonState = digitalRead(button);
   * RF24NetworkHeader header4(node012);                                      // (Address where the data is going)
   * bool ok3 = network.write(header4, &buttonState, sizeof(buttonState));    // Send the data
   *
   *COME RICEVERE
   *RF24NetworkHeader header;
   *unsigned long incomingData;
   *network.read(header, &incomingData, sizeof(incomingData)); // Read the incoming data
   */



      
   
