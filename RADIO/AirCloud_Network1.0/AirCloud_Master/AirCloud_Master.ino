    #include <RF24Network.h>
    #include <RF24.h>
    #include <SPI.h>

    RF24 radio(7, 8);                      // nRF24L01 (CE,CSN)
    RF24Network network(radio);            // Include the radio in the network
    const uint16_t this_node = 00;         // Indirizzo di questo nodo in forma ottale ( 04,031, etc)
    const uint16_t node01 = 01;            // Indirizzo degli altri nodi in forma ottale
    const uint16_t node02 = 02;
    
    unsigned long started_waiting_at = 0;
    boolean timeout = false;

    void setup()
    {
      SPI.begin();
      radio.begin();
      network.begin(90, this_node);               //(canale, indirizzo del nodo)
      radio.setDataRate(RF24_2MBPS);              //frequenza di funzionamento
      //myRadio.setPALevel(RF24_PA_MAX);          //potenza di funzionamento
      Serial.begin(9600);
    }
    
    void loop()
    {
      network.update();             //potrebbe non essere necessario

      int num = rand() % 5;         //genero un numero randomico da 1 a 10
      Serial.println(num);


      if(num == 0)
      {
        Serial.println(F("Ora invio"));     
        unsigned long start_time = micros();                                   // Prendo il tempo d'inzio comunicazione ed invio
        RF24NetworkHeader header01(01);        
        while(!network.write( header01, &start_time, sizeof(start_time) ))     //la funzione "write" restituisce un booleano in caso di ricezione(1) o meno(0)
        {
          Serial.println(F("Invio fallito"));                                  //Il ricevitore non ha ricevuto il segnale
        }    
    
     
       started_waiting_at = micros();                             // tempo corrente
       timeout = false;                                           // timeout -> true = ricezione fallita -> false = ricezione a buon fine
       while (!network.available() )                              // mentre non riceve nulla...
       {                                                           
         if (micros() - started_waiting_at > 2000000 )            // se aspetta più di 2 secondi, timeout = true, ed esci dal ciclo while
         {            
           timeout = true;
           break;
         }  
          network.update();                                       //aggiornare network (necessario)                 
       } 
        
       if ( timeout )
       {                                                           
         Serial.println(F("Invio fallito dello slave"));          //ricezione fallita -> lo slave non ha reinviato nulla
       }
       
       else
       {
         RF24NetworkHeader header;
         unsigned long got_time;                                       
         network.read(header, &got_time, sizeof(unsigned long) );   //leggi il valore in arrivo dallo slave
         unsigned long end_time = micros();
        
         Serial.print(F("Sent "));                                  //stampo tutti i risultati della comunicazione
         Serial.print(start_time);
         Serial.print(F(", Got response "));
         Serial.print(got_time);                                    //start_time e got_time devono essere uguali in una corretta comunicazione
         Serial.print(F(", Round-trip delay "));
         Serial.print(end_time-start_time);
         Serial.println(F(" microseconds"));
       }
     }

     if(num == 1)
      {
        Serial.println(F("Ora invio"));     
        unsigned long start_time = micros();                                   // Prendo il tempo d'inzio comunicazione ed invio
        RF24NetworkHeader header02(02);        
        while(!network.write( header02, &start_time, sizeof(start_time) ))     //la funzione "write" restituisce un booleano in caso di ricezione(1) o meno(0)
        {
          Serial.println(F("Invio fallito"));                                  //Il ricevitore non ha ricevuto il segnale
        }    
    
     
       started_waiting_at = micros();                             // tempo corrente
       timeout = false;                                           // timeout -> true = ricezione fallita -> false = ricezione a buon fine
       while (!network.available() )                              // mentre non riceve nulla...
       {                                                           
         if (micros() - started_waiting_at > 2000000 )            // se aspetta più di 2 secondi, timeout = true, ed esci dal ciclo while
         {            
           timeout = true;
           break;
         }  
          network.update();                                       //aggiornare network (necessario)                 
       } 
        
       if ( timeout )
       {                                                           
         Serial.println(F("Invio fallito dello slave"));          //ricezione fallita -> lo slave non ha reinviato nulla
       }
       
       else
       {
         RF24NetworkHeader header;
         unsigned long got_time;                                       
         network.read(header, &got_time, sizeof(unsigned long) );   //leggi il valore in arrivo dallo slave
         Serial.print(F("Stringa ... "));
         unsigned long end_time = micros();
        
         Serial.print(F("Sent "));                                  //stampo tutti i risultati della comunicazione
         Serial.print(start_time);
         Serial.print(F(", Got response "));
         Serial.print(got_time);                                    //start_time e got_time devono essere uguali in una corretta comunicazione
         Serial.print(F(", Round-trip delay "));
         Serial.print(end_time-start_time);
         Serial.println(F(" microseconds"));
       }
     }
     
     delay(1000);
   }


  /*
   * COME SCRIVERE
   * unsigned long buttonState = digitalRead(button);
   * RF24NetworkHeader header4(node012);                                      // (Address where the data is going)
   * bool ok3 = network.write(header4, &buttonState, sizeof(buttonState));    // Send the data
   *
   *COME RICEVERE
   *RF24NetworkHeader header;
   *unsigned long incomingData;
   *network.read(header, &incomingData, sizeof(incomingData)); // Read the incoming data
   */
