    #include <RF24Network.h>
    #include <RF24.h>
    #include <SPI.h>

    RF24 radio(7, 8);                      // nRF24L01 (CE,CSN)
    RF24Network network(radio);            // Include the radio in the network
    const uint16_t this_node = 00;         // Indirizzo di questo nodo in forma ottale ( 04,031, etc)
    const uint16_t node01 = 01;            // Indirizzo degli altri nodi in forma ottale
    const uint16_t node02 = 02;
    
    


    void setup()
    {
      SPI.begin();
      radio.begin();
      network.begin(90, this_node);               //(canale, indirizzo del nodo)
      radio.setDataRate(RF24_250KBPS);              //frequenza di funzionamento
      //myRadio.setPALevel(RF24_PA_MAX);          //potenza di funzionamento
      Serial.begin(9600);     
    }
      
void loop(void)
{
  // Pump the network regularly
  network.update();

  // Is there anything ready for us?
  while ( network.available() )
  {
    // If so, grab it and print it out

    int len=6;                                          //char receivedData[1];
    char gotmsg[6]="";
    RF24NetworkHeader header;
    //len = radio.getDynamicPayloadSize();
    network.read(header, &gotmsg, len );                        //RF24NetworkHeader header;
    String ciao = String (gotmsg);                                                    //network.read(header,receivedData,strlen(receivedData));
    Serial.print("Data Received - ");
    Serial.print(gotmsg);
    Serial.print(" - Size ");
    Serial.print(len);
    Serial.println();
    
  }
}


  /*
   * COME SCRIVERE
   * unsigned long buttonState = digitalRead(button);
   * RF24NetworkHeader header4(node012);                                      // (Address where the data is going)
   * bool ok3 = network.write(header4, &buttonState, sizeof(buttonState));    // Send the data
   *
   *COME RICEVERE
   *RF24NetworkHeader header;
   *unsigned long incomingData;
   *network.read(header, &incomingData, sizeof(incomingData)); // Read the incoming data
   */
