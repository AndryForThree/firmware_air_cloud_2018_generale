// Example 3 - Receive with start- and end-markers
#include "Seriale_AirCloud.h"
const byte numChars = 70;
char receivedChars[numChars];
Seriale seriale;
bool newData = 0;


void setup() {
    Serial.begin(115200);
    Serial.println("<Arduino is ready>");
}

void loop() {

    if (seriale.ricevi()){
      gestioneComunicazionePro (String(receivedChars));
    }
}


void gestioneComunicazionePro (String stringaRicevuta) {
  if (stringaRicevuta != ""){
    if (stringaRicevuta.indexOf("InizializzazioneSensore" == 0)){
      gestioneInizializzazione (stringaRicevuta.substring(24));
    }
  }
}


void gestioneInizializzazione (String stringaRicevuta){
  Serial.println (stringaRicevuta);
}
