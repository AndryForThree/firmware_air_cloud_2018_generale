#ifndef INCLUDES_SERIALE_AIRCLOUD_H_
#define INCLUDES_SERIALE_AIRCLOUD_H_


#include "Generale.h"
#include "Board.h"

extern class Generale generale;
extern int sensoriAirCloud[15][4];
extern int numeroSensoriAirCloud;
extern const byte numChars;
extern bool newData;
extern char receivedChars[];

class Seriale {

    public:
        bool ricevi ();
        void invio (String);
        void showNewData ();
    private:
        
};

#endif // _INCLUDES_RADIO_AIRCLOUD_H_ 
