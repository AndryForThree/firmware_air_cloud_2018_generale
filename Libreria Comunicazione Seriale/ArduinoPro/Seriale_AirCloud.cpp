#include "Seriale_AirCloud.h"

void Seriale::invio (String stringaDaInviare) {
  int len = stringaDaInviare.length() + 1;
  char sendData [50];
  stringaDaInviare.toCharArray(sendData, len);
  Serial.write(sendData, len); //Write the serial data
}

bool Seriale::ricevi() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '$';
    char endMarker = ';';
    char rc;
    bool ok;
    while (Serial.available() > 0 && newData == false) {
        ok = 1;
        rc = Serial.read();

        if (recvInProgress == true) {
            if (rc != endMarker) {
                receivedChars[ndx] = rc;
                ndx++;
                if (ndx >= numChars) {
                    ndx = numChars - 1;
                }
            }
            else {
                receivedChars[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                ndx = 0;
                newData = true;
            }
        }

        else if (rc == startMarker) {
            recvInProgress = true;
        }
    }
    if (ok) {
      return 1;
    }
    else return 0;
}




void Seriale::showNewData() {
    if (newData == true) {
        Serial.print("STRINGA RICEVUTA (Seriale)... ");
        Serial.println(receivedChars);
        newData = false;
    }
}
