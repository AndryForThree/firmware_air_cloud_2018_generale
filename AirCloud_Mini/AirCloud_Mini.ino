#include "Radio_AirCloud.h"

const uint16_t masterNode = 00;
const uint16_t thisNode = 01;
int stato = 1;
int temperatura = 15;
int sensore = 1;
int WDTMaxTimeRicezione = 2000;
int WDTMaxTimeInvio = 2000;
unsigned long WDTTimer = 0;
bool ok1;
bool ok2;

String sensorType = "temperatura";
String sensorPosition = "cucina";
String sensorData = String(temperatura, DEC);
String sensorNumber = String(sensore, DEC);


extern class Radio radio;
extern class Generale generale;

String stringaAttuale = "";
String stringaDesiderata = "$richiestaData, " + sensorNumber + ", " + sensorType + ", " + sensorPosition + ";";
String stringaOk = "$richiestaDataOk, " + sensorType + ", " + sensorPosition + ", " + sensorData + ";";
String stringaNak = "$richiestaDataNak, " + sensorType + ", " + sensorPosition + ";";



void setup() {
  Serial.begin(9600);
  delay(10);
}

void loop() {

switch(stato){
   
   case 1: //
   
    Serial.println ("inizio");
    ok1 = false;
    WDTTimer = millis ();
    while (( millis () - WDTTimer ) <= WDTMaxTimeRicezione && !ok1) {
      ok1 = radio.ricezione ();
    }
    if(ok1){
    Serial.println ("ricezione avvenuta"); 
      stato = 2; 
    }
    else{
    Serial.println ("nessuna ricezione");    
      stato = 1;
    }
    
    break;
    
  case 2: //
  
    Serial.println (stringaAttuale);
    ok2 = false;
    
    if (stringaAttuale.equals(stringaDesiderata)) {
      while(( millis () - WDTTimer ) <= WDTMaxTimeRicezione && !ok2){        //invioDati
        ok2 = radio.invio(stringaOk , masterNode);
      }
      if(ok2){
          Serial.println ("invioOk avvenuto");
        }
      else{
          Serial.println ("invioOk fallito");
        }
    }
    else {
      while(( millis () - WDTTimer ) <= WDTMaxTimeRicezione && !ok2){
        ok2 = radio.invio(stringaNak , masterNode);
      }
      if(ok2){
          Serial.println ("invioNak avvenuto");
      }
      else{
          Serial.println ("invioNak fallito");
      }
    }
    
    stato = 1;
    break;

}
}



/** 
   ESEMPIO DI COMUNICAZIONE dei sensori                 MAXI
x   "$richiestaDati, temperatura, cucina;"

   RIPOSTA DEL MINI                                     MINI
   "$dataOk, temperatura, cucina, 23;"    esci e vai avanti
   "$dataNak, temperatura, cucina;"       funzione ricorsiva
   ""                                     funzione ricorsiva
   
   RISPOSTA                                            MAXI
   "$dataOk;"                             esci e il MINI controlla che la comunicazione è arivata                                                       
   "dataNak;"                             Richiedo ritrasmissione
 * */
