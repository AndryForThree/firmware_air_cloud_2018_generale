#ifndef INCLUDES_RADIO_AIRCLOUD_H_
#define INCLUDES_RADIO_AIRCLOUD_H_

#include <RF24Network.h>
#include <RF24.h>
#include <SPI.h>


#include "Generale.h"

#include "Board.h"


extern int sensoriAirCloud[15][3];
extern int numeroSensoriAirCloud;

extern class Generale generale;

extern String stringaAttuale;

extern String sensorType;
extern String sensorPosition;




class Radio {

    public:
        Radio();
        void inizializzazione(const uint16_t);
        bool ricezione();
        bool inviaInizializzazione(const uint16_t, const uint16_t, String, String);
        bool gestioneStringheMaxi (String);
        bool gestioneStringheMini (String);
        bool inviaRichiestaDati(int);
        void richiediDati(int);
        bool invio(String, const uint16_t);

    private:
        
        void gestisciInizializzazioneMaxi (String);
        void gestioneComunicazioneSensoriMaxi (String);

        void gestisciInizializzazioneMini (String);
        void gestioneComunicazioneSensoriMini (String);
};

#endif // _INCLUDES_RADIO_AIRCLOUD_H_ 
