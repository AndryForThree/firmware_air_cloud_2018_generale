#include "Sensori_AirCloud.h"


#define DHTPIN 8    //definiamo il pin data
#define DHTTYPE DHT11  // definiamo il modello del nostro sensore
#define pwmPin 9    //definire pin per sensore co2
float hu;
float te;
float pm25;
float pm10;

long th, tl, h, l = 0.0;    //co2
long CO2 = 1500;
DHT dht(DHTPIN, DHTTYPE);


float p10, p25;
int error;


SDS011 my_sds;


Adafruit_CCS811 ccs;

// ROBA SUL PM SENSOR SENZA LIB
Sensori::Sensori(){

}

void Sensori::inizializzazione_Sensori()
{
  dht.begin();
  pinMode(pwmPin, INPUT);

  // INIZIALIZZAZIONE SENSORE VOC
  
  if(!ccs.begin()){
    Serial.println("Failed to start VOC sensor ! Please check your wiring.");
  }
}

float Sensori::ottieni_PM10()
{
  
 Serial1.flush();     // pulisce la seriale
 Serial1.flush();
 int startByte = 0;
 while(Serial1.available() == 0){}    // verifica la disponibilità dei dati  
 
 while(startByte != 170){      // segnala l'inizio di una stringa leggendo il byte d'inizio
    while(Serial1.available() == 0){}
    startByte = Serial1.read();

    //delay(1);
    }

    
  int byteArray10[10];
  int incomingByte = 0;
        //Serial.println("array PM10");
    for(int n=0; n <= 9; n++){   // memorizza in un array i valori di tutta la stringa (10 byte)
        while(Serial1.available() == 0){}   
        incomingByte = Serial1.read();
        byteArray10[n] = incomingByte;
        //Serial.println(byteArray10[n]);
        }



 String PM10String = String(byteArray10[4]) + String(byteArray10[3]);
 pm10 = PM10String.toInt();
 pm10 = pm10/10;
 
  return pm10;
}


float Sensori::ottieni_PM25()
{

 Serial1.flush();   // pulisce la seriale
 Serial1.flush();
  int startByte = 0;
 while(Serial1.available() == 0){}    // verifica la disponibilità dei dati 
 
 while(startByte != 170){      // segnala l'inizio di una stringa leggendo il byte d'inizio
  while(Serial1.available() == 0){} 
  startByte = Serial1.read(); 
  //delay(1);
  }

  
    int byteArray25[10]; 
    int incomingByte = 0;
    //Serial.println("array PM2.5");
    for(int n=0; n <= 8; n++){      // memorizza in un array i valori di tutta la stringa (10 byte)  
        while(Serial1.available() == 0){}
        incomingByte = Serial1.read();
        byteArray25[n] = incomingByte;
        //Serial.println(byteArray25[n]);
        }


 String PM25String = String(byteArray25[2]) + String(byteArray25[1]);
 pm25 = PM25String.toInt();
 pm25 = pm25/10;
 return pm25;
}

float Sensori::ottieni_Umidita()
{
  //float hu = dht.readHumidity(); // leggiamo il valore per l'umidita'
  return dht.readHumidity();
  //return hu;
}

float Sensori::ottieni_Temperatura()
{
  //float te = dht.readTemperature(); // leggiamo il valore per la temperatura
  return dht.readTemperature();
  //return te;
}

int Sensori::ottieni_VOC()
{
   /*if(ccs.available()){
    if(!ccs.readData()){
      return ccs.getTVOC();   
     }
    }*/
    //da decommentare qunando il sensore funzionerà

    return random(5,10);
}

long Sensori::ottieni_CO2()
{
  do {
        th = pulseIn(pwmPin, HIGH, 1004000) / 1000.0;
        tl = 1004 - th;
        CO2 = 2000 * (th-2)/(th+tl-4);
    } while (CO2 < 1.0);
  return CO2;
}
