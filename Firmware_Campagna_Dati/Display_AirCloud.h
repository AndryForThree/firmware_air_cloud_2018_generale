
#ifndef INCLUDES_DISPLAY_AIRCLOUD_H_
#define INCLUDES_DISPLAY_AIRCLOUD_H_

#endif // _INCLUDES_DISPLAY_AIRCLOUD_H_ 


#include <Adafruit_GFX.h>         //Display Nokia 
#include <Adafruit_PCD8544.h>     //Display Nokia

void inizializzazione_Display();
void display_Umidita(int);
void display_Temperatura(int);
void display_CO2(int);
