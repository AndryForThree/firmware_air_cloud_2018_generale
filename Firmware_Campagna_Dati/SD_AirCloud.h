
#ifndef INCLUDES_SD_AIRCLOUD_H_
#define INCLUDES_SD_AIRCLOUD_H_

#include <SPI.h>
#include <SD.h>
#include "Clock_AirCloud.h"



class AirCloud_SD{
    public:
        AirCloud_SD();
        void inizializzazione_SD();
        void scrivi_Legenda();
        void scrivi_Dati_Sensori(float, float, float, int, float, float);
    
    private:
        void apri_File();
        void chiudi_File();
        void invia_SD(String);
};



#endif // _INCLUDES_CLOCK_AIRCLOUD_H_ 
