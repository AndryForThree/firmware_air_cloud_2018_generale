#ifndef INCLUDES_CLOCK_AIRCLOUD_H_
#define INCLUDES_CLOCK_AIRCLOUD_H_

#include <Wire.h>
#include "RTClib.h"
#include <SPI.h>
#include <Adafruit_GFX.h>         //Display Nokia 
#include <Adafruit_PCD8544.h>     //Display Nokia



class Clock{

    public:
        Clock();
        void inizializzazione_Clock();
        String ottieni_Data_Orario();
        uint16_t ottieni_Anno();
        uint8_t ottieni_Mese();
        uint8_t ottieni_Giorno();
        uint8_t ottieni_Ora();
        uint8_t ottieni_Minuto();
        uint8_t ottieni_Secondo();
};

#endif  // _INCLUDES_CLOCK_AIRCLOUD_H_ 
