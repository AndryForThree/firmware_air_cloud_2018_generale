#include "Clock_AirCloud.h"

RTC_DS1307 RTC;

Clock::Clock(){
}

void Clock::inizializzazione_Clock()
{
  Wire.begin();
  RTC.begin();
  if (! RTC.isrunning()){
   Serial.println("RTC is NOT running!");
   RTC.adjust(DateTime(__DATE__, __TIME__)); // following line sets the RTC to the date & time this sketch was compiled
}
}

uint16_t Clock::ottieni_Anno(){
   return RTC.now().year();
}

uint8_t Clock::ottieni_Mese(){
  return RTC.now().month();
}

uint8_t Clock::ottieni_Giorno(){
   return RTC.now().day();
}

uint8_t Clock::ottieni_Ora(){
  return RTC.now().hour();
}

uint8_t Clock::ottieni_Minuto(){
  return RTC.now().minute();
}

uint8_t Clock::ottieni_Secondo(){
  return RTC.now().second();
}

String Clock::ottieni_Data_Orario(){
  
  String string_Anno    = String(ottieni_Anno());
  String string_Mese    = String(ottieni_Mese());
  String string_Giorno  = String(ottieni_Giorno());
  String string_Ora     = String(ottieni_Ora());
  String string_Minuto  = String(ottieni_Minuto());
  String string_Secondo = String(ottieni_Secondo());
  
  String string_Data_Orario = String(
    string_Anno     + "/"  +
    string_Mese     + "/"  +
    string_Giorno   + " "  +
    string_Ora      + ":"  +
    string_Minuto   + ":"  +
    string_Secondo );
  return string_Data_Orario; 
}
