 #include "Sensori_AirCloud.h"
 #include "Clock_AirCloud.h"
 #include "SD_AirCloud.h"

 float PM25 = 0;
 float PM10 = 0;

  Clock clock1;                  //inizializzazione libreria Clock
  AirCloud_SD AirCloud_SD;      //inzializzazione libreria SD
  Sensori Sensori;


void setup() 
{

  //  INIZIALIZZAZIONE PORTE SERIALI

  Serial.begin(9600);
  Serial1.begin(9600);    // inizializza la porta seriale numero 1 di Arduino Mega


  //  INIZIALIZZAZIONE GENERALI
  
  Sensori.inizializzazione_Sensori();     //inizializza la sensoristica utilizzata
  
  AirCloud_SD.inizializzazione_SD();           //inizializza la connessione con la scheda SD
  
  //inizializzazione_Display();     //inizializza il display utilizzato

  clock1.inizializzazione_Clock();          // inizializza l'RTC

  //display.begin();               //comandi per inizializzare il display
  //DisplayINIT();

/*
    //  RISCALDAMENTO

  Serial.println("inizio tempo di riscaldamento");
 for(int i = 3; i != 1; i--){
  Serial.print(i);
  Serial.println(" secondi alla fine");
  delay(1000);
 }
 Serial.println("1 secondo alla fine");
 delay(1000);
 Serial.println("tempo di riscaldamento finito");
*/
  
 //SCRITTURA NELLA SD DELLA STRINGA LEGENDA

   AirCloud_SD.scrivi_Legenda();               //( ATTENZIONE : commentare la stringa che scrive l'ora perchè funziona solo se l'RTC funziona)

}

void loop() {

  // ASSEGNAMENTO DELLA LETTURA AD UNA INT (per motivi di temporizzazione)
  PM25 = Sensori.ottieni_PM25();
  PM10 = Sensori.ottieni_PM10();

  // DATALOG DEI DATI DAI SENSORI CON RISPETTIVO ORARIO
  AirCloud_SD.scrivi_Dati_Sensori(Sensori.ottieni_Temperatura(),Sensori.ottieni_Umidita(),Sensori.ottieni_CO2(),Sensori.ottieni_VOC(),PM25, PM10);   // PM25 e PM10 saranno sostituiti con l'apposita funzione, una volta collegato il sensore

  delay(120000);         // per campagna dati


}
