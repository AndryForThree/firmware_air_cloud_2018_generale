
#include "Display_AirCloud.h"

//inizializzazioni per il display
Adafruit_PCD8544 display = Adafruit_PCD8544(3, 4, 5, 7, 6);

uint8_t picatura[8]   = {B00000001,B00000001,B00000001,B00000001,B00000110,B00000110,B00000110,B00000110};
uint8_t picatura2[8]  = {B10000000,B10000000,B10000000,B10000000,B01100000,B01100000,B01100000,B01100000};
uint8_t picatura3[8]  = {B00011000,B00011000,B00011000,B00011000,B00011000,B00011000,B00000111,B00000111};
uint8_t picatura4[8]  = {B00011000,B00011000,B00011000,B00011000,B00011000,B00011000,B11100000,B11100000};

uint8_t MEC[8]   = {B00000000,B00000000,B00000000,B00011100,B00011110,B00011111,B00001111,B00000111};
uint8_t MEC2[8]  = {B00000000,B00011000,B00111100,B00111100,B00111100,B00111100,B11111111,B11111111};
uint8_t MEC3[8]  = {B00000000,B00000000,B00000000,B00111000,B01111000,B11111000,B11110000,B11100000};
uint8_t MEC4[8]  = {B00000011,B00000011,B00111111,B01111111,B01111111,B00111111,B00000011,B00000011};
uint8_t MEC5[8]  = {B11111111,B11000011,B10000001,B10000001,B10000001,B10000001,B11000011,B11111111};
uint8_t MEC6[8]  = {B11000000,B11000000,B11111100,B11111110,B11111110,B11111100,B11000000,B11000000};
uint8_t MEC7[8]  = {B00000111,B00001111,B00011111,B00011110,B00011100,B00000000,B00000000,B00000000};
uint8_t MEC8[8]  = {B11111111,B11111111,B00111100,B00111100,B00111100,B00111100,B00011000,B00000000};
uint8_t MEC9[8]  = {B11100000,B11110000,B11111000,B01111000,B00111000,B00000000,B00000000,B00000000};

void inizializzazione_Display()
{
    display.begin();
    display.setContrast(50);
    display.clearDisplay();
    display.drawBitmap(30, 15,  MEC , 8, 8, 1);
    display.drawBitmap(38, 15,  MEC2, 8, 8, 1);
    display.drawBitmap(46, 15,  MEC3, 8, 8, 1);
    display.drawBitmap(30, 23,  MEC4, 8, 8, 1);
    display.drawBitmap(38, 23,  MEC5, 8, 8, 1);
    display.drawBitmap(46, 23,  MEC6, 8, 8, 1);
    display.drawBitmap(30, 30,  MEC7, 8, 8, 1);
    display.drawBitmap(38, 30,  MEC8, 8, 8, 1);
    display.drawBitmap(46, 30,  MEC9, 8, 8, 1);
    
   
    display.setTextSize(1);
    display.setTextColor(BLACK);
    display.setCursor(27,2);
    display.print("I.I.S");
 
    display.setTextSize(1);
    display.setTextColor(BLACK);
    display.setCursor(5,40);
    display.print("VOLTERRA ELIA");
    display.display();

    delay (3000);
    display.clearDisplay();

    display.setTextSize(2);
    display.setTextColor(BLACK);
    display.setCursor(12,8);
    display.print("i");

    display.setTextSize(1);
    display.setTextColor(BLACK);
    display.setCursor(22,17);
    display.print("r");
    
    display.setTextSize(2);
    display.setTextColor(BLACK);
    display.setCursor(28,8);
    display.print("C");

    display.setTextSize(1);
    display.setTextColor(BLACK);
    display.setCursor(39,14);
    display.print("ONTROL");

    display.setTextSize(1);
    display.setTextColor(BLACK);
    display.setCursor(5,35);
    display.print("Air.Q.Control");
    
    display.display();

    delay (5000);
    display.clearDisplay();
}


void display_Temperatura(int valore_Temperatura)
{
   display.clearDisplay(); 
   display.setTextSize(1);
   display.setTextColor(BLACK);
   display.setCursor(9,2);
   display.print("TEMPERATURE");
   
   display.setTextSize(4);
   display.setTextColor(BLACK);
   display.setCursor(0,18);
   display.print(valore_Temperatura);
   
   display.setCursor(59,18);
   display.print("C");
   
   display.setTextSize(2);
   display.setTextColor(BLACK);
   display.setCursor(47,18);
   display.print("o");
   
   display.display();
}

void display_Umidita(int valore_Umidita)
{
   display.clearDisplay(); 

   display.setTextSize(4);
   display.setCursor(0,18);
   display.print(valore_Umidita);
   
   display.drawBitmap(70, 27,  picatura , 8, 8, 1);
   display.drawBitmap(78, 27,  picatura2, 8, 8, 1);
   display.drawBitmap(70, 35,  picatura3, 8, 8, 1);
   display.drawBitmap(78, 35,  picatura4, 8, 8, 1);

   display.setTextSize(1);
   display.setTextColor(BLACK);
   display.setCursor(18,2);
   display.print("HUMIDITY");
   
  
   display.setTextSize(3);
   display.setTextColor(BLACK);
   display.setCursor(52,24);
   display.print("%");
   display.display();
}

void display_CO2(int valore_CO2)
{
   display.clearDisplay();  

   display.setTextSize(1);
   display.setCursor(18,2);
   display.setTextColor(BLACK);
   display.print("CO  [ppm]");
   
   display.setTextSize(0);
   display.setCursor(30,5);
   display.setTextColor(BLACK);
   display.print("2");
   
   display.setTextSize(3);
   display.setTextColor(BLACK);
      if(valore_CO2 > 0 && valore_CO2<1000 )
      {
         display.setCursor(16,24);
      }
      else
      {
         display.setCursor(7,24);
      }
  
   display.print(valore_CO2);
   
   display.display();
}

//void display_Date(int valore_Date){}
//void display_Time(int valore_Time){}
//void display_CO(int valore_CO){}
//void display_PM25(int valore_PM25){}
//void display_PM10(int valore_PM10){}


