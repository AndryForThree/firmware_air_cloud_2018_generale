
#include "SD_AirCloud.h"

Clock Clock;

const int chipSelect = 4;
File dataFile;

AirCloud_SD::AirCloud_SD(){

}

void AirCloud_SD::inizializzazione_SD()
{
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }


  Serial.print("Initializing SD card...");

  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    return;
  }
  Serial.println("card initialized.");
}


void AirCloud_SD::scrivi_Legenda()
{
  String stringa_Legenda = ("Date/Time"   "\t"
                            "Temp"        "\t"
                            "Hum"         "\t"
                            "CO2"         "\t"
                            "VOC"          "\t"    
                            "PM2.5"       "\t"
                            "PM10"            );
                                                                   
  invia_SD(stringa_Legenda);
  Serial.println(stringa_Legenda);
}


void AirCloud_SD::scrivi_Dati_Sensori(float temperatura, float umidita, float CO2, int VOC, float PM25, float PM10)
{


  String tempString  = String(temperatura);
  String humString   = String(umidita);
  String CO2String   = String(CO2);
  String VOCString    = String(VOC);
  String PM25String  = String(PM25);
  String PM10String  = String(PM10);
  
  String stringa_Dati = (Clock.ottieni_Data_Orario()  + "\t" +    // COMMENTA LA STRINGA SE NON USI L'RTC
                         tempString             + "\t" +
                         humString              + "\t" +
                         CO2String              + "\t" +
                         VOCString               + "\t" +
                         PM25String             + "\t" +
                         PM10String                     );
   
   invia_SD(stringa_Dati);
   Serial.println(stringa_Dati);
}

void AirCloud_SD::apri_File(){
  dataFile = SD.open("AirCloud.txt", FILE_WRITE );
  // Serial.println("File creato/aperto");
}



void AirCloud_SD::chiudi_File()
{
  dataFile.close();
  // Serial.println("File chiuso");
}


void AirCloud_SD::invia_SD(String stringa_Da_Inviare)
{
  apri_File();
  dataFile.println(stringa_Da_Inviare);
  chiudi_File();
  // Serial.println("Stringa loggata");
}
