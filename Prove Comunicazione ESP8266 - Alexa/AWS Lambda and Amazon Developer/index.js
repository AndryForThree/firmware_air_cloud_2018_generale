1//per far funzionare le skill alexa
const Alexa = require('alexa-sdk');
const { PerformanceObserver, performance } = require('perf_hooks');

//per far funzionare le funzioni di comunicazione con dweet.io
const https = require('https');
var URL = require('url').URL;

//variabili per comunicazione con dweet.io
const dweetioDeviceName = "AirCloud"
const dweetioSensorType = "divisionePerTipoSensore"
const dweetioSensorposition = "divisionePerPosizioneSensore"

//const speechOutput = this.event.request.intent.slots.Sensore.value;
var outputAlexaSpeak;
const APP_ID = undefined;



const handlers = {
    'LaunchRequest': function() {
        this.response.speak("Ciao, come posso aiutarti?")
            .listen();
        this.emit(':responseReady');
    },

    'getData': function() {

        var sensorType = this.event.request.intent.slots.sensorType.value;
        var sensorPosition = this.event.request.intent.slots.sensorPosition.value;

        var options = new URL('https://dweet.io/get/latest/dweet/for/' + dweetioDeviceName + ',' + dweetioSensorType);


        reciveDataDweetio(options, (function(recivedPacket1) {

            if (recivedPacket1.content[sensorType].indexOf(sensorPosition) != -1) { //è stato riconosciuto che quel tipo di sensore è all'interno di quella stanza
                options = new URL('https://dweet.io/get/latest/dweet/for/' + dweetioDeviceName + ',' + sensorType + ',' + sensorPosition);

                reciveDataDweetio(options, (function(recivedPacket) {
                    if (recivedPacket.thing == (dweetioDeviceName + ',' + sensorType + ',' + sensorPosition)) {

                        if (recivedPacket.content.sensorType == sensorType) {
                            if (recivedPacket.content.sensorPosition == sensorPosition) {

                                outputAlexaSpeak = createAlexaOutputSpeechGetData(sensorPosition, sensorType, recivedPacket.content.data)

                            }
                            else {
                                outputAlexaSpeak = "errore: non è stato riconosciuta la posizione de sensore";
                            }
                        }
                        else {
                            outputAlexaSpeak = "errore: non è stato riconosciuto il tipo del sensore";
                        }
                    }
                    else {
                        outputAlexaSpeak = "errore: non è stato riconosciuto il nome del dweet.io";
                    }
                    this.response.speak(outputAlexaSpeak + ", vuoi che ti dica qualcos'altro?")
                    .listen("vuoi che ti dica qualcos'altro?")
                    this.emit(':responseReady');
                }).bind(this));
            }
            else { //il sensore richiesto non è all'interno della stanza
                var sensorPositionOfRequest = recivedPacket1.content[sensorType];
                var elementsInThatPosition = getNumberOfElementsOfString(sensorPositionOfRequest);

                if (elementsInThatPosition == 0) { //non ci sono sensori di quel tipo nel sistema
                    outputAlexaSpeak = "Mi dispiace, non esistono sensori di " + sensorType + " collegati al tuo AirCloud";
                }
                else {
                    outputAlexaSpeak = "Mi dispiace, non ho trovato alcun sensore di " + sensorType + " in " + sensorPosition + ", forse potrebbe interessarti la " + sensorType;
                    if (elementsInThatPosition == 1) { //c'è solamente un sensore nel sistema
                        outputAlexaSpeak = outputAlexaSpeak + " in " + getFirstElementOfString(sensorPositionOfRequest, 1);
                    }
                    else { //ci sono più sensori nel sistema
                        for (var elementsRemaining = elementsInThatPosition; elementsInThatPosition != 0; elementsInThatPosition--) {
                            var currentElement = getFirstElementOfString(sensorPositionOfRequest, elementsInThatPosition);

                            if (elementsRemaining != elementsInThatPosition) { //non siamo nel primo ciclo, quinid non serve la virgola
                                if (elementsInThatPosition == 1) { //è rimasto solo un elemento, quini serve inserire un oppure alla fine della frase
                                    outputAlexaSpeak = outputAlexaSpeak + " oppure in " + currentElement;
                                }
                                else { //sono rimasti più elementi, quindi basta un "in"
                                    outputAlexaSpeak = outputAlexaSpeak + ", in " + currentElement;
                                }
                            }
                            else {
                                outputAlexaSpeak = outputAlexaSpeak + " in " + currentElement;
                            }
                            sensorPositionOfRequest = deleteFirstElementOfString(sensorPositionOfRequest);
                        }
                    }
                }
                this.response.speak(outputAlexaSpeak)
                    .listen();
                this.emit(':responseReady');
            }
        }).bind(this));
    },

    'getRoomSituation': function() { //funzione per ricevere i dati di tutti i sensori nella stanza
        
        var sensorPosition = this.event.request.intent.slots.sensorPosition.value;
        var options = new URL('https://dweet.io/get/latest/dweet/for/' + dweetioDeviceName + ',' + dweetioSensorposition);
        
        reciveDataDweetio(options, (function(recivedPacket) {
            if (recivedPacket.content[sensorPosition] != "") { //sono presenti dei sensori nella stanza richiesta
                
                var sensorString = recivedPacket.content[sensorPosition];
                var sensorStringLenght = getNumberOfElementsOfString(sensorString);
                var sensorArray = ["","","","","","","","","",""];
                
                for(var i = 0; i < sensorStringLenght; i++){
                    sensorArray[i] = getFirstElementOfString(sensorString, (sensorStringLenght - i));
                    sensorString = deleteFirstElementOfString(sensorString, (sensorStringLenght - i));
                }
                
                options = new URL('https://dweet.io/get/latest/dweet/for/' + dweetioDeviceName + ',' + sensorArray[0] + ',' + sensorPosition)
                reciveDataDweetio(options,(function(sensorData1){
                    
                    sensorStringLenght--;
                    if(sensorStringLenght > 0){
                        options = new URL('https://dweet.io/get/latest/dweet/for/' + dweetioDeviceName + ',' + sensorArray[1] + ',' + sensorPosition)
                        reciveDataDweetio(options,(function(sensorData2){
                           
                            sensorStringLenght--;
                            if(sensorStringLenght > 0){
                                options = new URL('https://dweet.io/get/latest/dweet/for/' + dweetioDeviceName + ',' + sensorArray[2] + ',' + sensorPosition)
                                reciveDataDweetio(options,(function(sensorData3){
                                    
                                    sensorStringLenght--;
                                    if(sensorStringLenght > 0){
                                        options = new URL('https://dweet.io/get/latest/dweet/for/' + dweetioDeviceName + ',' + sensorArray[3] + ',' + sensorPosition)
                                        reciveDataDweetio(options,(function(sensorData4){
                                            
                                            sensorStringLenght--;
                                            if(sensorStringLenght > 0){
                                                options = new URL('https://dweet.io/get/latest/dweet/for/' + dweetioDeviceName + ',' + sensorArray[4] + ',' + sensorPosition)
                                                reciveDataDweetio(options,(function(sensorData5){
                                                    
                                                    sensorStringLenght--;
                                                    if(sensorStringLenght > 0){
                                                        options = new URL('https://dweet.io/get/latest/dweet/for/' + dweetioDeviceName + ',' + sensorArray[5] + ',' + sensorPosition)
                                                        reciveDataDweetio(options,(function(sensorData6){
                                                            
                                                            sensorStringLenght--;
                                                            if(sensorStringLenght > 0){
                                                                options = new URL('https://dweet.io/get/latest/dweet/for/' + dweetioDeviceName + ',' + sensorArray[6] + ',' + sensorPosition)
                                                                reciveDataDweetio(options,(function(sensorData7){
                                                                    
                                                                    sensorStringLenght--;
                                                                    if(sensorStringLenght > 0){
                                                                        options = new URL('https://dweet.io/get/latest/dweet/for/' + dweetioDeviceName + ',' + sensorArray[7] + ',' + sensorPosition)
                                                                        reciveDataDweetio(options,(function(sensorData8){
                                                                            sensorStringLenght--;
                                                                            if(sensorStringLenght > 0){
                                                                                options = new URL('https://dweet.io/get/latest/dweet/for/' + dweetioDeviceName + ',' + sensorArray[8] + ',' + sensorPosition)
                                                                                reciveDataDweetio(options,(function(sensorData9){
                                                                                    
                                                                                    sensorStringLenght--;
                                                                                    if(sensorStringLenght > 0){
                                                                                        options = new URL('https://dweet.io/get/latest/dweet/for/' + dweetioDeviceName + ',' + sensorArray[9] + ',' + sensorPosition)
                                                                                        reciveDataDweetio(options,(function(sensorData10){
                                                                                            outputAlexaSpeak = createAlexaOutputSpeechGetRoomSituation(sensorData1, sensorData2, sensorData3, sensorData4, sensorData5, sensorData6, sensorData7, sensorData8, sensorData9, sensorData10, 10, sensorPosition);
                                                                                            this.response.speak(outputAlexaSpeak)
                                                                                                .listen();
                                                                                            this.emit(':responseReady');
                                                                                        }).bind(this));
                                                                                    }
                                                                                    else{
                                                                                        outputAlexaSpeak = createAlexaOutputSpeechGetRoomSituation(sensorData1, sensorData2, sensorData3, sensorData4, sensorData5, sensorData6, sensorData7, sensorData8, sensorData9, "", 9, sensorPosition);
                                                                                        this.response.speak(outputAlexaSpeak)
                                                                                            .listen();
                                                                                        this.emit(':responseReady');
                                                                                    }
                                                                                }).bind(this));
                                                                            }
                                                                            else{
                                                                                outputAlexaSpeak = createAlexaOutputSpeechGetRoomSituation(sensorData1, sensorData2, sensorData3, sensorData4, sensorData5, sensorData6, sensorData7, sensorData8, "", "", 8, sensorPosition);
                                                                                this.response.speak(outputAlexaSpeak)
                                                                                    .listen();
                                                                                this.emit(':responseReady');
                                                                            }
                                                                        }).bind(this));
                                                                    }
                                                                    else{
                                                                        outputAlexaSpeak = createAlexaOutputSpeechGetRoomSituation(sensorData1, sensorData2, sensorData3, sensorData4, sensorData5, sensorData6, sensorData7, "", "", "", 7, sensorPosition);
                                                                        this.response.speak(outputAlexaSpeak)
                                                                            .listen();
                                                                        this.emit(':responseReady');
                                                                    }
                                                                }).bind(this));
                                                            }
                                                            else{
                                                                outputAlexaSpeak = createAlexaOutputSpeechGetRoomSituation(sensorData1, sensorData2, sensorData3, sensorData4, sensorData5, sensorData6,"", "", "", "", 6, sensorPosition);
                                                                this.response.speak(outputAlexaSpeak)
                                                                    .listen();
                                                                this.emit(':responseReady');
                                                                
                                                            }
                                                        }).bind(this));
                                                    }
                                                    else{
                                                        outputAlexaSpeak = createAlexaOutputSpeechGetRoomSituation(sensorData1, sensorData2, sensorData3, sensorData4, sensorData5, "","", "", "", "", 5, sensorPosition);
                                                        this.response.speak(outputAlexaSpeak)
                                                            .listen();
                                                        this.emit(':responseReady');
                                                    }
                                                }).bind(this));
                                            }
                                            else{
                                                outputAlexaSpeak = createAlexaOutputSpeechGetRoomSituation(sensorData1, sensorData2, sensorData3, sensorData4, "", "","", "", "", "", 4, sensorPosition);
                                                this.response.speak(outputAlexaSpeak)
                                                    .listen();
                                                this.emit(':responseReady');
                                                
                                            }
                                        }).bind(this));
                                    }
                                    else{
                                        outputAlexaSpeak = createAlexaOutputSpeechGetRoomSituation(sensorData1, sensorData2, sensorData3, "", "", "","", "", "", "", 3, sensorPosition);
                                        this.response.speak(outputAlexaSpeak)
                                            .listen();
                                        this.emit(':responseReady');
                                    }
                                }).bind(this));
                            }
                            else{//2
                                outputAlexaSpeak = createAlexaOutputSpeechGetRoomSituation(sensorData1, sensorData2, "", "", "", "","", "", "", "", 2, sensorPosition);
                                this.response.speak(outputAlexaSpeak)
                                    .listen();
                                this.emit(':responseReady');
    
                            }
                        }).bind(this));
                    }
                    else{
                        outputAlexaSpeak = createAlexaOutputSpeechGetRoomSituation(sensorData1, "", "", "", "", "","", "", "", "", 1, sensorPosition);
                        this.response.speak(outputAlexaSpeak)
                            .listen();
                        this.emit(':responseReady');
                    }
                }).bind(this));
            }
            else{
                outputAlexaSpeak = "Mi dispiace, non è presente alcun sensore in " + sensorPosition + ", prova a inserirne qualcuno oppure chiedimi qualcos'altro"
                this.response.speak(outputAlexaSpeak)
                    .listen();
                this.emit(':responseReady');
            }
        }).bind(this));
    },
    'AMAZON.StopIntent': function() {
        this.response.speak("vabene, ci sentiamo alla prossima");
        this.emit(':responseReady');
    }
};

exports.handler = function(event, context, callback) {
    const alexa = Alexa.handler(event, context, callback);
    alexa.APP_ID = APP_ID;
    alexa.registerHandlers(handlers);
    alexa.execute();
};

//ricezione dei dati scritti sul cloud dweet.io

function reciveDataDweetio(options, callback) {
    https.get(options, function(res) {

        res.on('data', function(d) {
            process.stdout.write(d);
            var jsonObjectRecived = JSON.parse(d);
            var goodJson = getJsonReadable(jsonObjectRecived);
            callback(goodJson);
        });

        res.on('error', function(e) {
            console.error(e);
        });

    });
}

function sendDataDweetio(requestType, sensorType, sensorPosition) {
    var url = 'https://dweet.io/dweet/for/' + dweetioDeviceName + '?requestType=' + requestType + '&sensorType=' + sensorType + '&sensorPosition=' + sensorPosition;
    console.log(url);
    https.get(url, (res) => {

        res.on('data', (d) => {
            process.stdout.write(d);
        });

    }).on('error', (e) => {
        console.error(e);
    });
}


function getJsonReadable(jsonObjectRecived) {

    var valoreRicevutoStringa = JSON.stringify(jsonObjectRecived); //tagliare la partte iniziale che è inutile
    var StringaTagliata = valoreRicevutoStringa.substring(58, valoreRicevutoStringa.length - 2); //tagliare la parentesi quadra alla posizione
    return JSON.parse(StringaTagliata);

}

function createAlexaOutputSpeechGetData(sensorPosition, sensorType, data) {

    var outputAlexaSpeak = "";

    switch (sensorType) {

        case "temperatura":
            outputAlexaSpeak = "La temperatura in " + sensorPosition + " è di " + data + " gradi centigradi";
            break;

        case "umidita":
            outputAlexaSpeak = "L'umidità in " + sensorPosition + " è del " + data + "per cento";
            break;

        case "CO2":
            outputAlexaSpeak = "La C O 2 in " + sensorPosition + " è di" + data + "parti per milione";
            break;

        case "VOC":
            outputAlexaSpeak = "I composti organici volanti in " + sensorPosition + " sono di" + data + "";
            break;

        case "pm25":
            outputAlexaSpeak = "Le polveri sottili pm25 in " + sensorPosition + " sono di" + data + "microgrammi per metro cubo";
            break;

        case "pm10":
            outputAlexaSpeak = "Le polveri sottili pm10 in " + sensorPosition + " sono di" + data + "microgrammi per metro cubo";
            break;

        case "metano":
            outputAlexaSpeak = "Il metano in " + sensorPosition + " è di " + data;
    }

    return outputAlexaSpeak;
}

/*function getRightSensorInPosition(sensorType, recivedPacket){
   switch (sensorType){
       case "temperatura":
           return recivedPacket.content.temperatura;
           
        case "umidita":
            return recivedPacket.content.umidita;
           
        case "CO2":
            return recivedPacket.content.CO2;
         
         case "VOC":
            return recivedPacket.content.VOC;
           
         case "pm10":
            return recivedPacket.content.pm10;
         
        case "pm25":
            return recivedPacket.content.pm25;
   }
}*/

function getNumberOfElementsOfString(string) {
    var string2 = string.match(/,/g);
    if (string2 == null) {
        if (string === "") {
            return 0;
        }
        else {
            return 1;
        }
    }
    else {
        var ciao = string.match(/,/g).length + 1;
        return ciao;
    }
}

function getFirstElementOfString(string, elementsRemaninig) {
    if (elementsRemaninig != 1) {
        return string.substring(0, string.indexOf(","))
    }
    else {
        return string;
    }
}

function deleteFirstElementOfString(string, elementsRemaining) {
    return string.substring(string.indexOf(",") + 1, string.length);

}

function createAlexaOutputSpeechGetRoomSituation(data1, data2, data3, data4, data5, data6, data7, data8, data9, data10, numberOfData, sensorPosition){
    var string = "Ecco a te la situazione in " + sensorPosition + ": Primo Sensore." + rightArticle(data1.content.sensorType) + data1.content.sensorType + " è di " + data1.content.data + rightMesurementUnit(data1.content.sensorType) + " .\n ";
    var data = ["","","","","","","","","",""]
    data[0] = data1;
    data[1] = data2;
    data[2] = data3;
    data[3] = data5;
    data[4] = data6;
    data[5] = data7;
    data[6] = data8;
    data[7] = data8;
    data[8] = data9;
    data[9] = data10;
    console.log(data1.content);
    for(var i = 1; i < numberOfData; i++){
        string = string + numerazioneSensori(i +1) + " sensore." + rightArticle(data[i].content.sensorType) + data[i].content.sensorType + " è di " + data[i].content.data + rightMesurementUnit(data[1].content.sensorType) + ".\n ";
    }

    return string + " vuoi sapere altro?";
}

function numerazioneSensori(number){
    switch (number){
        case 1:
        return "Primo";

        case 2:
        return "Secondo";
        
        case 3:
        return "Terzo";
        
        case 4:
        return "Quarto";
        
        case 5:
        return "Quito";
        
        case 6:
        return "Sesto";
        
        case 7:
        return "Settimo";
        
        case 8:
        return "Ottavo";
        
        case 9:
        return "Nono";
        
        case 10:
        return "Decimo";
    }
}

function rightArticle (sensorType){
    if(sensorType == "temperatura" || sensorType == "VOC" || sensorType == "CO2"){
        return " la ";
    }
    
    if(sensorType == "umidita"){
        return " l' ";
    }
    
    if(sensorType == "pm10" || sensorType == "pm25"){
        return " i ";
    }
        
    if(sensorType == "metano"){
        return " il ";
    }
}

function rightMesurementUnit (sensorType){

         switch (sensorType) {

        case "temperatura":
            return " gradi centigradi ";

        case "umidita":
            return " per cento "

        case "CO2":
            return " parti per milione "

        case "VOC":
            return " "

        case "pm25":
            return " microgrammi per metro cubo "

        case "pm10":
            return " microgrammi per mtro cubo "

        case "metano":
            return ""
    }
}