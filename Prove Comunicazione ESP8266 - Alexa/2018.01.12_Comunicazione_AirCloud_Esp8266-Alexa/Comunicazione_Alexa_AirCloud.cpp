#include "Comunicazione_Alexa_AirCloud.h"


//password WIFi
/*const char* ssid = "alexa";
const char* password = "Al3x@_18";*/
/*
const char* ssid = "FASTWEB-1-P6Wv8v5hsa1s";
const char* password = "2ugQvNwrNt";*/

const char* ssid = "Andrea";
const char* password = "12345678";

//comunicazione http
const char* host = "dweet.io";
WiFiClient client;

//variabile per riconoscere se sia arrivato un nuovo dweet
  short newDweetFlag = 0;
	
  String real_thing;
  String real_content;
  

  String real_content_requestType;
  String real_content_sensorType;
  String real_content_sensorPosition;


  /*String stringaSensoriTemperatura;
  String stringaSensoriUmidita;	
  String stringaSensoriCO2;
  String stringaSensoriPm10;
  String stringaSensoriPm25;
  String stringaSensoriVOC;*/

  //enum dei sensori
  enum tipiSensori{
    temperatura = 1,
    umidita = 2,
    CO2 = 3,
    VCO = 4,
    pm10 = 5,
    pm25 = 6,
    metano = 7
  };

  //enum delle posizioni possibili
   enum posizioniSensori {
    salotto = 1,
    cucina = 2,
    bagno = 3,
    cameraDaLetto = 4,
    sgabuzzino = 5,
    garage = 6,
    soffitta = 7,
    cantina = 8
  };

void wifiConnection(){
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
}
  
void httpConnectionToHost(){
 // Use WiFiClient class to create TCP connections
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }
}
 
void sendDataToDweetio(String communicationType, String requestType, String sensorType, String sensorPosition, int sensorData){
  // This will send the request to the server
  httpConnectionToHost();
  String url;
  if(communicationType == "Aggiornamento pagina sensore singolo"){
    url = creazioneStringaDaInviareAggiornamntoSensori(requestType, sensorType, sensorPosition, sensorData);
  }
  else {
    if(communicationType == "aggiornamento file per divisione sensori"){
        url = creazioneStringaDaInviareFileSensori(creazioneListaPosizioni(temperatura), creazioneListaPosizioni(umidita), creazioneListaPosizioni(CO2), creazioneListaPosizioni(VCO), creazioneListaPosizioni(pm10), creazioneListaPosizioni(pm25), creazioneListaPosizioni(metano));
    }
    else{
      if(communicationType == "aggiornamento file per divisone posizioni"){
        Serial.println("ciao");
         url = creazioneStringaDaInviareFilePosizione(creazioneListaSensori(salotto), creazioneListaSensori(cucina), creazioneListaSensori(bagno), creazioneListaSensori(cameraDaLetto), creazioneListaSensori(sgabuzzino), creazioneListaSensori(garage), creazioneListaSensori(cantina), creazioneListaSensori(soffitta));
      }
    }
  }
  
  Serial.println(url);
  client.print(url);
  delay(10);
}

String creazioneListaPosizioni (int tipoSensore){
  int elementiPresenti = trovaLaPrimaCellaLibera(tipoSensore, "tipo") - 1;
  String stringaPosizioneDeiSensori = "";
  for(int i = 0; i < elementiPresenti + 1; i++){
    stringaPosizioneDeiSensori = stringaPosizioneDeiSensori + trasformaEnumPosizioneInStringa(divisionePerTipo[tipoSensore][i]) + ",";
  } 
  //int stringaPosizioneDeiSensoriLunghezza = stringaPosizioneDeiSensori.length() - 1;
  stringaPosizioneDeiSensori = stringaPosizioneDeiSensori.substring(0, (stringaPosizioneDeiSensori.length() - 1));
  //Serial.println(stringaPosizioneDeiSensori);
  return stringaPosizioneDeiSensori;
}

String creazioneListaSensori (int posizioneSensore){
    int elementiPresenti = trovaLaPrimaCellaLibera(posizioneSensore, "posizione") - 1;
    String stringaSensoriPresenti = "";
    for(int i = 0; i < elementiPresenti + 1; i++){
        stringaSensoriPresenti = stringaSensoriPresenti + trasformaEnumTipoInStringa(divisionePerPosizione[posizioneSensore][i]) + ",";
    }
    stringaSensoriPresenti = stringaSensoriPresenti.substring(0, (stringaSensoriPresenti.length() -1));
    //Serial.println(stringaSensoriPresenti);
    return stringaSensoriPresenti;
}

String trasformaEnumPosizioneInStringa(int posizioneSensore){
  switch (posizioneSensore){
    case 1:
    return "salotto";

    case 2:
    return "cucina";
    
    case 3:
    return "bagno";
    
    case 4:
    return "cameraDaLetto";
    
    case 5:
    return "sgabuzzino";
    
    case 6:
    return "garage";
    
    case 7:
    return "soffitta";

    case 8:
    return "cantina";
  }
}

String trasformaEnumTipoInStringa(int tipoSensore){
    switch (tipoSensore){
        case 1:
        return "temperatura";

        case 2:
        return "umidita";
        
        case 3:
        return "CO2";
        
        case 4:
        return "VCO";
        
        case 5:
        return "pm10";
        
        case 6:
        return "pm25";
        
        case 7:
        return "metano";
    }
}

String reciveDataFromDweetio(String url){
  //ricezione di dati dweet.io
  HTTPClient http;    //Declare object of class HTTPClient
  http.begin(url);     //Specify request destination
  int httpCode = http.GET();            //Send the request
  String stringaRicevuta = http.getString();    //Get the response payload
  return stringaRicevuta.substring(58,(stringaRicevuta.length()-2));
}

/**crezione di una stringa URL per la comuniucazione con dweet.io
 * La stringa posiziona nel nome dell'oggetto dweetio il nome AirCloud e a seguire
 * il tipo del sensore e la sua posizione. In questo modo ogni sensore nel sitema ha una sua pagina 
 * personale, per evitare sovvraccarichi del server dweetio
 * */
String creazioneStringaDaInviareAggiornamntoSensori(String requestType, String sensorType, String sensorPosition, int sensorData){
  return String("GET /dweet/for/") + nomeDispositivo + "," + sensorType + "," + sensorPosition +
                      "?requestType=" + requestType +
                      "&sensorType="  + sensorType + 
                      "&sensorPosition="  + sensorPosition + 
                      "&data="  + String(sensorData) + 
                      " HTTP/1.1\r\n" +
                      "Host: " + host + "\r\n" + 
                      "Connection: close\r\n\r\n";
      
}
//da completare
String creazioneStringaDaInviareFilePosizione (String stringSalotto, String stringCucina, String stringBagno, String stringCameraDaLetto, String stringSgabuzzino, String stringGarage, String stringSoffitta, String stringCantina){
  return String("GET /dweet/for/") + nomeDispositivo + ",divisionePerPosizioneSensore" +
                      "?salotto=" + stringSalotto +
                      "&cucina="  + stringCucina +
                      "&bagno="  + stringBagno + 
                      "&cameraDaLetto="  + stringCameraDaLetto + 
                      "&sgabuzzino="  + stringSgabuzzino + 
                      "&garage="  + stringGarage + 
                      "&soffitta="  + stringSoffitta + 
                      "&cantina="  + stringCantina + 
                      " HTTP/1.1\r\n" +
                      "Host: " + host + "\r\n" + 
                      "Connection: close\r\n\r\n";
}

/**creazione di una stringa di comunuicazione URL per la creazione di un 
 * file ijn cui siano riportatio tutti i sensori del sistama, divisiin base
 * al loro tipo. 
 * */
String creazioneStringaDaInviareFileSensori(String sensorTemperatura, String sensorUmidita, String sensorCO2, String sensorVCO, String sensorPm10, String sensorPm25, String sensorMetano){
  return String("GET /dweet/for/") + nomeDispositivo + ",divisionePerTipoSensore" +
                      "?temperatura=" + sensorTemperatura +
                      "&umidita="  + sensorUmidita + 
                      "&CO2="  + sensorCO2 + 
                      "&VCO="  + sensorVCO +
                      "&pm10="  + sensorPm10 +
                      "&pm25="  + sensorPm25 +
                      "&metano="  + sensorMetano +
                      " HTTP/1.1\r\n" +
                      "Host: " + host + "\r\n" + 
                      "Connection: close\r\n\r\n";
}

int reciveFromDweetio(){
	//bisogna creare l'url di ricezione
  String urlRicezione = "http://dweet.io/get/latest/dweet/for/" + nomeDispositivo;
	String stringaRicevuta = reciveDataFromDweetio(urlRicezione);

 //settaggio dei buffer per il json
  const size_t capacity = JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(4) + 282;
  DynamicJsonBuffer jsonBuffer(capacity);
  
  //ricezione dei dati e trasformazione in json
  JsonObject& root = jsonBuffer.parseObject(stringaRicevuta);
  Serial.print("stringa ricevuta: ");
  Serial.println(stringaRicevuta);
  
  String thing = "";
  String created = "";
  
  root["thing"].printTo(thing);
  root["created"].printTo(created);

  JsonObject& content = root["content"];

  String content_requestType = "";
  String content_sensorType = "";
  String content_sensorPosition = "";
  
  content["requestType"].prettyPrintTo(content_requestType);
  content["sensorType"].printTo(content_sensorType);
  content["sensorPosition"].printTo(content_sensorPosition);


  real_thing = thing.substring(1,(thing.length()-1));
  real_content = created.substring(1,(created.length()-1));
  
  real_content_requestType = content_requestType.substring(1,(content_requestType.length() - 1));
  real_content_sensorType = content_sensorType.substring(1, (content_sensorType.length() - 1));
  real_content_sensorPosition = content_sensorPosition.substring(1,(content_sensorPosition.length() - 1));
  
	  enum communications {
	  getData = 1,
	  comandoNonRiconosciuto = 2,
	  nomeDispositivoNonCorrrispondente = 3,
	  nessunNuovoDweet = 4,
	  sendDataToLambdaFromEsp8266 = 5
	};

  //scelta del comando da ritornare, in base a quanto ricevuto da dweetio
   if (real_thing == "AirCloud") {
  	 if (real_content_requestType == "getData") {
  		 return getData;
  	 } 
  	 else {
  		 if (real_content_requestType == "sendDataToLambdaFromEsp8266") {
  			 return sendDataToLambdaFromEsp8266;
  		 }
  		 else {
  			 return comandoNonRiconosciuto;
  		 }
  	 }
   }
   else {
  	return nomeDispositivoNonCorrrispondente;
   }	  
}


String jsonAirCloud_get(String jsonRequest){
	if(jsonRequest == "requestType"){
		return real_content_requestType;
	}
	else {
		if(jsonRequest == "sensorType"){
			return real_content_sensorType;
		}
		else {
			if(jsonRequest == "sensorPosition") {
				return real_content_sensorPosition; 
			}
		}
	}
}

void creazioneArraySensori(){
  for(short i = 0; i < 10; i++){
    int Ciao = sensoriAirCloud[i][0];
    int Ciao1 = trovaLaPrimaCellaLibera(Ciao, "tipo");
    divisionePerTipo[Ciao][Ciao1] = sensoriAirCloud[i][1];
  }
}

void creazioneArrayPosizioni(){
  for(short i = 0; i < 10; i++){
    int Ciao = sensoriAirCloud[i][1];
    int Ciao1 = trovaLaPrimaCellaLibera(Ciao, "posizione");
    divisionePerPosizione[Ciao][Ciao1] = sensoriAirCloud[i][0];
  }
}
 
int trovaLaPrimaCellaLibera (int filaInCuiCercare, String arrayDoveCercare){
  if(arrayDoveCercare == "tipo"){
    for(int i = 0; i < 5; i++){
        if(divisionePerTipo[filaInCuiCercare][i] == 0) return i;
    }
  }
  if(arrayDoveCercare == "posizione"){
    for(int i = 0; i < 10; i++){
        if(divisionePerPosizione[filaInCuiCercare][i] == 0) return i;
    }
  }
  
}
