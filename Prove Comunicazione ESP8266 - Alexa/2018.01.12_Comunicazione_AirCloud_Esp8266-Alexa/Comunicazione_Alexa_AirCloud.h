#ifndef INCLUDES_COMUNICAZIONE_ALEXA_AIRCLOUD_H_
#define INCLUDES_COMUNICAZIONE_ALEXA_AIRCLOUD_H_

#endif /* _INCLUDES_COMUNICAZIONE_ALEXA_AIRCLOUD_H_ */


#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>

#include <ArduinoJson.h>

extern String nomeDispositivo;
extern int sensoriAirCloud[15][2];
extern int divisionePerTipo [8][5];
extern int divisionePerPosizione [9][10];

void wifiConnection();
void httpConnectionToHost();
String creazioneStringaDaInviareAggiornamntoSensori(String, String, String, int);       //creazione della stringa da inviare a dweet.io
String creazioneStringaDaInviareFileSensori(String, String, String, String, String, String, String);
String creazioneStringaDaInviareFilePosizione(String, String, String, String, String, String, String, String);
String creazioneListaPosizioni(int);
String creazioneListaSensori(int);
void sendDataToDweetio(String, String, String, String, int);                 //invio dei dati verso dweet.io
String reciveDataFromDweetio(String);
int reciveFromDweetio();
String jsonAirCloud_get(String);
short isThereNewDweet();
void creazioneArraySensori();
void creazioneArrayPosizioni();
int trovaLaPrimaCellaLibera(int, String);
String trasformaEnumPosizioneInStringa(int);
String trasformaEnumTipoInStringa(int);
