
  // Import required libraries
#include "Comunicazione_Alexa_AirCloud.h"

int temperatura_salotto = 15;
int temperatura_cucina = 20;
int temperatura_bagno = 25;
int matano_cucina = 200;
int metano_garage = 250;
int CO2_cameraDaLetto = 200;
int umidita_bagno = 20;
int umidita_garage = 50;

int sensorData = 0;
String nomeDispositivo = "AirCloud";
long tempo_prec;
 
 //enum dei sensori
  enum tipiSensori{
    temperatura = 1,
    umidita = 2,
    CO2 = 3,
    VCO = 4,
    pm10 = 5,
    pm25 = 6,
    metano = 7
  };

  //enum delle posizioni possibili
   enum posizioniSensori {
    salotto = 1,
    cucina = 2,
    bagno = 3,
    cameraDaLetto = 4,
    sgabuzzino = 5,
    garage = 6,
    soffitta = 7,
    cantina = 8
  };
//inizializzazione dei sensori disponibili
int sensoriAirCloud[15][2] = {
    {temperatura,cucina},
    {temperatura,salotto},
    {metano,cucina},
    {metano,garage},
    {umidita,bagno},
    {CO2,cameraDaLetto},
    {temperatura, bagno},
    {umidita,garage}
  };

  //inizializzazioni degli array multidimensionali per gestire organizzare i sensori
  int divisionePerTipo [8][5] = {};               /**il primo numero indica il numero di tipi di sensore, l'ordine è riportato nel commento seguente
                                                  *il secondo numero indica il numero massimo di sensori mini aircloud per tipo nel sistema
                                                  *per esempio ci possono essere al massimo 5 sensori di temperatura in tutto il sistema aircloud
                                                  */
                                                  /**ORDINE DEI SENSORI
                                                    * temperatura
                                                    * umidita
                                                    * CO2
                                                    * VCO
                                                    * pm10
                                                    * pm25
                                                    * metano
                                                    * */
int divisionePerPosizione [9][10] = {};           /**ORDINE DELLE posizioni
                                                    *salotto
                                                    * cucina
                                                    * bagno
                                                    * CameraDaLetto
                                                    * sgabuzzino
                                                    * garage
                                                    * soffitta
                                                    * cantina
                                                    * */

enum communications {
  getData = 1,
  comandoNonRiconosciuto = 2,
  nomeDispositivoNonCorrrispondente = 3,
  nessunNuovoDweet = 4,
  sendDataToLambdaFromEsp8266 = 5
};

void setup() {
   Serial.begin(115200);
   delay(100);                   //da provare se si puo togliere

   //connessione al wifi
   wifiConnection();

   delay(10);
   creazioneArraySensori();
   creazioneArrayPosizioni();

   sendDataToDweetio("aggiornamento file per divisione sensori","","","",15);
   sendDataToDweetio("aggiornamento file per divisone posizioni", "","","",12);
}

void loop() {
  
  temperatura_salotto++;
  if(temperatura_salotto == 30)   temperatura_salotto = 15;

  temperatura_bagno++;
  if(temperatura_cucina == 30)   temperatura_cucina = 15;

  temperatura_bagno++;
  if(temperatura_bagno == 30)   temperatura_bagno = 15;

  matano_cucina = matano_cucina + 10;
  if(matano_cucina == 500)      matano_cucina = 200;

  metano_garage = metano_garage + 10;
  if(metano_garage == 500)   metano_garage = 200;

  CO2_cameraDaLetto = CO2_cameraDaLetto + 20;
  if(CO2_cameraDaLetto == 700)   CO2_cameraDaLetto = 250;

 umidita_bagno = umidita_bagno + 5;
  if(umidita_bagno == 100)   umidita_bagno = 10;

 umidita_garage = umidita_garage + 5;
  if(umidita_garage == 100)   umidita_garage = 10;
  
  
  sendDataToDweetio("Aggiornamento pagina sensore singolo", "getData", "temperatura", "salotto", temperatura_salotto);
  sendDataToDweetio("Aggiornamento pagina sensore singolo", "getData", "temperatura", "cucina", temperatura_cucina);
  sendDataToDweetio("Aggiornamento pagina sensore singolo", "getData", "temperatura", "bagno", temperatura_bagno);
  sendDataToDweetio("Aggiornamento pagina sensore singolo", "getData", "metano", "cucina", matano_cucina);
  sendDataToDweetio("Aggiornamento pagina sensore singolo", "getData", "metano", "garage", metano_garage);
  sendDataToDweetio("Aggiornamento pagina sensore singolo", "getData", "CO2", "cameraDaLetto", CO2_cameraDaLetto);
  sendDataToDweetio("Aggiornamento pagina sensore singolo", "getData", "umidita", "bagno", umidita_bagno);
  sendDataToDweetio("Aggiornamento pagina sensore singolo", "getData", "umidita", "garage", umidita_garage);
  

  delay(10000);
 
}
