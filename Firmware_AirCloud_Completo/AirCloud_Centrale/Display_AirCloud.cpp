#include "Display_AirCloud.h"

enum tipiSensori{
    temperatura = 1,
    umidita = 2,
    CO2 = 3,
    VCO = 4,
    pm10 = 5,
    pm25 = 6,
    metano = 7
  };

  //enum delle posizioni possibili
   enum posizioniSensori {
    salotto = 1,
    cucina = 2,
    bagno = 3,
    cameraDaLetto = 4,
    sgabuzzino = 5,
    garage = 6,
    soffitta = 7,
    cantina = 8,
    
  };

Display::Display(){
  
}


String Display::ricezione () {
  String stringa = "";
  if (Serial.available() > 0){
    while (Serial.available() > 0) {
      char ciao = Serial.read();
      stringa = stringa + String (ciao, HEX);
      delay (1);
    }
    return stringa;
  }
  else {
    return "-1";
  }
}


void Display::gestioniComunicazioniSensori (String stringaRicevuta){
  if (stringaRicevuta != "-1"){
    if (stringaRicevuta.indexOf("66") == 0){
      Serial.println("");
      Serial.print (stringaRicevuta);
      Serial.write (0xff);
      Serial.write (0xff);
      Serial.write (0xff);
      gestionePagineDisplay(stringaRicevuta.substring (2,3).toInt());
    }
  }
}


void Display::gestionePagineDisplay (int paginaSelezionata){
  int val = 0;
  Serial.println("");
  Serial.println (paginaSelezionata);
  Serial.write (0xff);
  Serial.write (0xff);
  Serial.write (0xff);
  switch (paginaSelezionata) {
    case 0:        
    
    break;
  
    case 1:         
    {
    String listaPosizioni = alexa.creazioneListaPosizioni (temperatura);
    inviaInzializzazioneDisplay (listaPosizioni);
    inviaDatiSensori(temperatura, listaPosizioni);
    break;
    }
    
    case 2:           //CO2
    {
    String listaPosizioni = alexa.creazioneListaPosizioni (umidita);
    inviaInzializzazioneDisplay (listaPosizioni);
    inviaDatiSensori(umidita, listaPosizioni);
    break;
    }

    case 3:           //PM10-25
    {
    String listaPosizioni = alexa.creazioneListaPosizioni (CO2);
    inviaInzializzazioneDisplay (listaPosizioni);
    inviaDatiSensori(CO2, listaPosizioni);
    break;
    }

    case 4:           //VCO
    {
    String listaPosizioni = alexa.creazioneListaPosizioni (pm10);
    inviaInzializzazioneDisplay (listaPosizioni);
    inviaDatiSensori(pm10, listaPosizioni);
    break;
    }

    case 5:           //VCO
    {
    String listaPosizioni = alexa.creazioneListaPosizioni (VCO);
    inviaInzializzazioneDisplay (listaPosizioni);
    inviaDatiSensori(VCO, listaPosizioni);
    break;
    }
  }
}


void Display::invioToDisplay (String stringaDaInviare) {
    Serial.print (stringaDaInviare);
    Serial.write (0xff);
    Serial.write (0xff);
    Serial.write (0xff);
}


void Display::inviaInzializzazioneDisplay (String listaPosizioni) {
  
  int numeroSensori = generale.getNumberOfElementsOfString(listaPosizioni);
  invioToDisplay("numeroSensori.val=" + String(numeroSensori));
  int numeroSensoreAttuale = 1;
  
  while (numeroSensori != 0){
    String sensoreAttuale = generale.getFirstElementOfString(listaPosizioni, 0);
    listaPosizioni = generale.deleteFirstElementOfString(listaPosizioni, 0);
    String stringaDaInviare =  "sensor" + String(numeroSensoreAttuale) + "Name.txt=\"" + sensoreAttuale + "\"";
    invioToDisplay(stringaDaInviare);
    numeroSensori--;
    numeroSensoreAttuale++;
  }
}

void Display::inviaDatiSensori (int tipoSensore, String listaPosizioni) {

  int numeroSensori = generale.getNumberOfElementsOfString(listaPosizioni);
  int numeroSensoreAttuale = 1;
  
  while (numeroSensori != 0) {
   
  String sensoreAttuale = generale.getFirstElementOfString(listaPosizioni, 0);
  listaPosizioni = generale.deleteFirstElementOfString(listaPosizioni, 0);
  int rigaSensore = cercaSensoriFromPositionAndType(tipoSensore, generale.stringaPosizioneToEnum(sensoreAttuale));
  if (tipoSensore == pm10){
    String stringaDaInviare = "sensor" + String(numeroSensoreAttuale) + "Value1.val=" + String(sensoriAirCloud[rigaSensore][4]);
    invioToDisplay(stringaDaInviare);
    stringaDaInviare = "sensor" + String(numeroSensoreAttuale) + "Value2.val=" + String(sensoriAirCloud[rigaSensore + 1][4]);
    invioToDisplay(stringaDaInviare);
  }
  else{
    String stringaDaInviare = "sensor" + String(numeroSensoreAttuale) + "Value.val=" + String(sensoriAirCloud[rigaSensore][4]);
    invioToDisplay(stringaDaInviare);
  }
  
  numeroSensori--;
  numeroSensoreAttuale++;
  }
}



int Display::cercaSensoriFromPositionAndType (int tipoSensore, int posizioneSensore){
  for (int i = 0; i < 15; i++) {
    if (tipoSensore == sensoriAirCloud[i][0]) {
      if (posizioneSensore == sensoriAirCloud[i][1]){
        return i;
      }
    }
  }
}
