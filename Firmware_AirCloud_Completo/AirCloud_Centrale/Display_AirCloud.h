#ifndef INCLUDES_DISPLAY_AIRCLOUD_H_
#define INCLUDES_DISPLAY_AIRCLOUD_H_

#include "Board.h"
#include "Comunicazione_Alexa_AirCloud.h"

extern int sensoriAirCloud[15][5];
extern int divisionePerTipo [8][5];
extern int divisionePerPosizione [9][10];

extern class Generale generale;
extern class Alexa alexa;

class Display {
    public:
        Display();
        String ricezione();
        void gestioniComunicazioniSensori(String);
        void gestionePagineDisplay(int);
        void invioToDisplay(String);
        void inviaInzializzazioneDisplay(String);
        void inviaDatiSensori(int, String);
        int cercaSensoriFromPositionAndType(int, int);
    private:

};



#endif
