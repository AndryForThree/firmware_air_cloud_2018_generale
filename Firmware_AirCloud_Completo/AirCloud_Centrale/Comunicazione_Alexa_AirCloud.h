#ifndef INCLUDES_COMUNICAZIONE_ALEXA_AIRCLOUD_H_
#define INCLUDES_COMUNICAZIONE_ALEXA_AIRCLOUD_H_

#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

#include "Board.h"
#include "Generale.h"


extern String nomeDispositivo;
extern int sensoriAirCloud[15][5];
extern int divisionePerTipo [8][5];
extern int divisionePerPosizione [9][10];
extern int numeroSensoriAirCloud;

extern class Radio radio;
extern class Generale generale;


class Alexa {
  
public:
  Alexa();
  void wifiConnection();
  void httpConnectionToHost();
  void sendDataToDweetio(String, String, String, String, int);
  void creazioneArraySensori();
  void creazioneArrayPosizioni();
  String creazioneListaPosizioni(int);
  String creazioneListaSensori(int);

private:
  String creazioneStringaDaInviareAggiornamntoSensori(String, String, String, int);       //creazione della stringa da inviare a dweet.io
  String creazioneStringaDaInviareFileSensori(String, String, String, String, String, String, String);
  String creazioneStringaDaInviareFilePosizione(String, String, String, String, String, String, String, String);
  String reciveDataFromDweetio(String);
  int reciveFromDweetio();
  String jsonAirCloud_get(String);
  short isThereNewDweet();
  
};

#endif /* _INCLUDES_COMUNICAZIONE_ALEXA_AIRCLOUD_H_ */
