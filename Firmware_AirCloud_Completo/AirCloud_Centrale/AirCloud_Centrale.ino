// Import required libraries


#include "Comunicazione_Alexa_AirCloud.h"
#include "Radio_AirCloud.h"
#include "Generale.h"
#include "Display_AirCloud.h"

int sensorData = 0;
String nomeDispositivo = "AirCloud";
long tempoPrec;

extern class Radio radio;
extern class Alexa alexa;
extern class Generale generale;
extern class Display display;

//cose necessarie per la libreria radio
  const uint16_t this_node = 00; 
  String stringaAttuale = "";
  int numeroSensori = -1;

//enum dei sensori
  enum tipiSensori{
    temperatura = 1,
    umidita = 2,
    CO2 = 3,
    VCO = 4,
    pm10 = 5,
    pm25 = 6,
    metano = 7
  };

  //enum delle posizioni possibili
   enum posizioniSensori {
    salotto = 1,
    cucina = 2,
    bagno = 3,
    cameraDaLetto = 4,
    sgabuzzino = 5,
    garage = 6,
    soffitta = 7,
    cantina = 8
  };

int numeroMassimoSensori = 15;

//inizializzazione dei sensori disponibili
//{temperatura, cucina, indirizzo, numero, datoDaSensore}
int sensoriAirCloud[15][5] = {
    /*{temperatura,cucina,1,1,0},
    {umidita,salotto,1,1,0},
    {VCO,garage,1,1,0},
    {VCO,soffitta,1,1,0},
    {umidita,bagno,1,1,0},
    {CO2,cantina,1,1,0},
    {pm10, bagno,1,1,0},
    {pm25,bagno,1,1,0}*/
  };
  
int numeroSensoriAirCloud = 0;


/*{
    {temperatura,cucina},
    {temperatura,salotto},
    {metano,cucina},
    {metano,garage},
    {umidita,bagno},
    {CO2,cameraDaLetto},
    {temperatura, bagno},
    {umidita,garage}
  };*/


  

  //inizializzazioni degli array multidimensionali per gestire organizzare i sensori
  int divisionePerTipo [8][5] = {};               /**il primo numero indica il numero di tipi di sensore, l'ordine è riportato nel commento seguente
                                                  *il secondo numero indica il numero massimo di sensori mini aircloud per tipo nel sistema
                                                  *per esempio ci possono essere al massimo 5 sensori di temperatura in tutto il sistema aircloud
                                                  */
                                                  /**ORDINE DEI SENSORI
                                                    * temperatura
                                                    * umidita
                                                    * CO2
                                                    * VCO
                                                    * pm10
                                                    * pm25
                                                    * metano
                                                    * */
int divisionePerPosizione [9][10] = {};           /**ORDINE DELLE posizioni
                                                    *salotto
                                                    * cucina
                                                    * bagno
                                                    * CameraDaLetto
                                                    * sgabuzzino
                                                    * garage
                                                    * soffitta
                                                    * cantina
                                                    * */

enum communications {
  getData = 1,
  comandoNonRiconosciuto = 2,
  nomeDispositivoNonCorrrispondente = 3,
  nessunNuovoDweet = 4,
  sendDataToLambdaFromEsp8266 = 5
};
  int datoSensore1Temp = 23;
  int datoSensore2Um = 65;
  int datoSensore3VCO = 1;
  int datoSensore4VCO = 1;
  int datoSensore5Um = 73;
  int datoSensore6CO2 = 423;
  int datoSensore7Pm10 = 11;
  int datoSensore8Pm25 = 23;

void setup() {
   Serial.begin(9600);
   delay(100);                   //da provare se si puo togliere
    
   //Serial.println ("INIZIALIZZAZIONE AIRCLOUD MAXI");
   //connessione al wifi
   //alexa.wifiConnection();
   delay(10);
   
   alexa.creazioneArraySensori();
   alexa.creazioneArrayPosizioni();
   
   
   
   radio.inizializzazione(this_node);
   tempoPrec = millis();
   Serial.println("Inizializzazione");
}


 /**LOGICA PROGRAMMA COMPLETO
   * Controllare che qualche AirCloud Mini voglia comunincare con il Maxi
   * Se c'è una richiesta Svolgere operazioni di setup per lo specifico AirCloud Mini
   * entrare in polling chiedendo ad ogni mini lo specifico dato che deve comunicare
   * comunicare a dweet.io i dati ricevuti
   * svolgere funzioni relative ai display e vari
   * */

int sensore = 0;
void loop() {
  bool ok = radio.ricezione();
  if(ok) {
      radio.gestioneStringheMaxi(stringaAttuale);
    }
  
  if ((millis() - tempoPrec) > 5000) {
    tempoPrec = millis();
    if(numeroSensori != -1){
      if (sensore == numeroSensori) {
       sensore = -1;
      }
      sensore++;
      //Serial.println (sensore);
      radio.inviaRichiestaDati (sensore);
    }
    
  }

//tutta la sezione random
  /*if ((millis () - tempoPrec) > 5000){
    tempoPrec = millis();
    if(random(5,15) >= 10) {
      datoSensore1Temp = datoSensore1Temp + 1;
    }
    else{
     datoSensore1Temp = datoSensore1Temp - 1;
    }

    if (datoSensore1Temp >= 25){
      datoSensore1Temp = 25;
    }
    if(datoSensore1Temp <= 20){
      datoSensore1Temp = 20;
    }
    sensoriAirCloud[0][4] = datoSensore1Temp;
    

    if(random(5,15) >= 10) {
      datoSensore2Um = datoSensore2Um + 1;
    }
    else{
      datoSensore2Um = datoSensore2Um - 1;
    }
    if (datoSensore2Um >= 77){
      datoSensore2Um = 77;
    }
    if(datoSensore2Um <= 53){
      datoSensore2Um = 53;
    }
    sensoriAirCloud[1][4] = datoSensore2Um;


    if(random(5,15) >= 10) {
      datoSensore3VCO = datoSensore3VCO + 1;
    }
    else{
      datoSensore3VCO = datoSensore3VCO - 1;
    }
    if (datoSensore3VCO >= 2){
      datoSensore3VCO = 2;
    }
    if(datoSensore3VCO <= 0){
      datoSensore3VCO = 0;
    }
    sensoriAirCloud[2][4] = datoSensore3VCO;
    

    if(random(5,15) >= 10) {
      datoSensore4VCO = datoSensore4VCO + 1;
    }
    else{
      datoSensore4VCO = datoSensore4VCO - 1;
    }
    if (datoSensore4VCO >= 2){
      datoSensore4VCO = 2;
    }
    if(datoSensore4VCO <= 0){
      datoSensore4VCO = 0;
    }
    sensoriAirCloud[3][4] = datoSensore4VCO;


    if(random(5,15) >= 10) {
      datoSensore5Um = datoSensore5Um + 1;
    }
    else{
      datoSensore5Um = datoSensore5Um - 1;
    }
    if (datoSensore5Um >= 77){
      datoSensore5Um = 77;
    }
    if(datoSensore5Um <= 53){
      datoSensore5Um = 53;
    }
    sensoriAirCloud[4][4] = datoSensore5Um;

    if(random(5,15) >= 10) {
      datoSensore6CO2 = datoSensore6CO2 + 3;
    }
    else{
      datoSensore6CO2 = datoSensore6CO2 - 3;
    }
    if (datoSensore6CO2 >= 903){
      datoSensore6CO2 = 903;
    }
    if(datoSensore6CO2 <= 357){
      datoSensore6CO2 = 357;
    }
    sensoriAirCloud[5][4] = datoSensore6CO2;


    if(random(5,15) >= 10) {
      datoSensore7Pm10 = datoSensore7Pm10 + 1;
    }
    else{
      datoSensore7Pm10 = datoSensore7Pm10 - 1;
    }
    if (datoSensore7Pm10 >= 26){
      datoSensore7Pm10 = 26;
    }
    if(datoSensore7Pm10 <= 5){
      datoSensore7Pm10 = 5;
    }
    sensoriAirCloud[6][4] = datoSensore7Pm10;


    if(random(5,15) >= 10) {
      datoSensore8Pm25 = datoSensore8Pm25 + 1;
    }
    else{
      datoSensore8Pm25 = datoSensore8Pm25 - 1;
    }
    if (datoSensore8Pm25 >= 37){
      datoSensore8Pm25 = 37;
    }
    if(datoSensore8Pm25 <= 9){
      datoSensore8Pm25 = 9;
    }
    sensoriAirCloud[7][4] = datoSensore8Pm25;
    
  }*/
  
  //gestione display
  String stringaRicevuta = display.ricezione();
  display.gestioniComunicazioniSensori (stringaRicevuta);
}
