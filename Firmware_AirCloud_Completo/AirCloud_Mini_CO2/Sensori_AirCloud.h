#ifndef INCLUDES_SENSORI_AIRCLOUD_H_
#define INCLUDES_SENSORI_AIRCLOUD_H_

#include <SDS011.h>
#include <DHT.h>
#include "Adafruit_CCS811.h"

class Sensori {
    public:
        Sensori();
        void inizializzazione_Sensori();
        float ottieni_PM10();
        float ottieni_PM25();
        float ottieni_Umidita();
        float ottieni_Temperatura();
        int ottieni_VOC();       //uint16_t in uscita quando il sensore funziona
        long ottieni_CO2();
};


#endif /* INCLUDES_SENSORI_AIRCLOUD_H_ */