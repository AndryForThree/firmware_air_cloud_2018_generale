#include "Radio_AirCloud.h"

RF24 radioOriginale(2, 15);               // nRF24L01 (CE,CSN)
RF24Network network(radioOriginale);     // Include the radio in the network


long WDTMaxTimeRicezione = 2000;
int livello = 0;


//enum dei sensori
enum tipiSensori {
  temperatura = 1,
  umidita = 2,
  CO2 = 3,
  VCO = 4,
  pm10 = 5,
  pm25 = 6,
  metano = 7
};

//enum delle posizioni possibili
enum posizioniSensori {
  salotto = 1,
  cucina = 2,
  bagno = 3,
  cameraDaLetto = 4,
  sgabuzzino = 5,
  garage = 6,
  soffitta = 7,
  cantina = 8
};


Radio::Radio() {

}


void Radio::inizializzazione(const uint16_t this_node) {
  SPI.begin();
  radioOriginale.begin();
  network.begin(90, this_node);               //(channel, node address)
  radioOriginale.setDataRate(RF24_250KBPS);            //oppure setDataRate(RF24_250KBPS); quale conviene?
  radioOriginale.setPALevel(RF24_PA_MAX);          //potenza di funzionamento
}


bool Radio::invio(String stringaDaInviare, const uint16_t nodeToSend) {
  network.update();
  
  int len = stringaDaInviare.length();
  char sendData [50];
  stringaDaInviare.toCharArray(sendData, len);
  
  RF24NetworkHeader header1(nodeToSend);
  Serial.print("Stringa inviata: ");
  Serial.println(stringaDaInviare);
  bool ok = network.write(header1, &sendData, sizeof(sendData));
  //Serial.println ("ciao");
  return ok;
}


bool Radio::ricezione() {
  network.update();
  
  if ( network.available() )                              //da cambiare con un if
  {
    int len = 150;                                          //char receivedData[1];
    char gotmsg[150] = "";

    RF24NetworkHeader header;

    network.read(header, &gotmsg, sizeof(gotmsg)); 
    String Ciao = String (gotmsg);
    stringaAttuale = Ciao;
    Serial.print ("Stringa ricevuta: ");
    Serial.println (stringaAttuale);
    return 1;
  }
  return 0;
}


/**
 
   ESEMPIO COMUNICAZIONE DI INIZALIZZAZIONE
   "$inizializzazione, temperatura, cucina, 01"
   RISPOSTA
   "$inizializzazioneOk;"
   
 * */
 short Radio::inviaInizializzazione(const uint16_t nodeToSend, const uint16_t thisNode, String sensorType, String sensorPosition, short stato) {
  
  switch (stato) {
  
  case 0:   //preparazione delle stringhe e invio
  {
  //Serial.println(thisNode);
  String stringaDaInviare = "$inizializzazione," + sensorType + "," + sensorPosition + "," + String(thisNode) + ";";
  
  bool ok = 0;
  while (!ok) {
    delay (500);
    ok = invio (stringaDaInviare, nodeToSend);
  }
  return 1;
  break;
  }

  
  case 1:   //ricezione della RISPOSTA
  {
  bool ok = 0;
  while (!ok){
    ok = ricezione ();
  }
  
  if (ok == 1) {        //ho ricevuto qualcosa
    if (stringaAttuale.indexOf("$inizializzazioneOk") == 0) {
      stringaAttuale = generale.deleteFirstElementOfString(stringaAttuale, 0);
      numeroSensore = stringaAttuale.toInt();
      stringaAttuale = "";
      return 2;
    }
    else {
      Serial.println ("Ho ricevuto qualcosa di sbagliato");
      return 0;
    }
  }
  else {
    Serial.println ("Non ho ricevuto nulla");
    return 0;
  }
  break;
  } 
  }
}


bool Radio::inviaRichiestaDati(int numeroSensore) {
  uint16_t indirizzoSensore = sensoriAirCloud[numeroSensore][3];
  return invio("richiesta dati", indirizzoSensore);
}


int Radio::gestioneStringheMaxi (String stringaRicevuta) {
  if (stringaRicevuta.indexOf("inizializzazione") == 1) {                     //controllo se la stringa ricevuta è una stringa di inzializzazione
    gestisciInizializzazioneMaxi (stringaRicevuta.substring(1));
    return 1;
  }
  else {
    if (stringaRicevuta.indexOf("data") == 1)    {
      gestioneComunicazioneSensoriMaxi (stringaRicevuta.substring(1));
      return 2;
    }
    else return 0;                                                                //stringa ricevuta non è di inizializ
  }
}

bool Radio::gestioneStringheMini(String stringaRicevuta) {
if (stringaRicevuta.indexOf("richiestaDati") == 1) {
  int stato = 0;
  while (stato != 4) {
    stato = gestioneComunicazioneSensoriMini(stringaRicevuta.substring(15), stato);
  }
  return 1;
}
  else return 0;                                                                //stringa ricevuta non è di inizializ
}




void Radio::gestioneComunicazioneSensoriMaxi (String stringaRicevuta) {
  if (stringaRicevuta.indexOf("Ok") == 0){        //ho ricevuto una comunicazione di ok

    stringaRicevuta = generale.deleteFirstElementOfString(stringaRicevuta, 0);
    int rigaSensore = generale.cercaSensore(generale.getFirstElementOfString(stringaRicevuta, 0));
    stringaRicevuta = generale.deleteFirstElementOfString(stringaRicevuta, 0);
    
    if (generale.getFirstElementOfString(stringaRicevuta, 0) == generale.trasformaEnumTipoInStringa(sensoriAirCloud[rigaSensore][0])) {
      generale.deleteFirstElementOfString(stringaRicevuta, 0);
      
      if (generale.getFirstElementOfString(stringaRicevuta, 0) == generale.trasformaEnumPosizioneInStringa(sensoriAirCloud[rigaSensore][1])) {
        sensoriAirCloud[rigaSensore][4] = stringaRicevuta.toInt();
      }
      else {
        invio ("$dataNak;", sensoriAirCloud [rigaSensore][2]);
      }
    }
    else {
      invio ("$dataNak;", sensoriAirCloud [rigaSensore][2]);
    }
  }
  else {
    stringaRicevuta = generale.deleteFirstElementOfString(stringaRicevuta, 0);
    int rigaSensore = generale.cercaSensore(generale.getFirstElementOfString(stringaRicevuta, 0));
    invio ("$dataNak;", sensoriAirCloud [rigaSensore][2]);
  }
}



/** 
   ESEMPIO DI COMUNICAZIONE dei sensori                 MAXI
   "$richiestaDati,1,temperatura, cucina"

   RIPOSTA DEL MINI                                     MINI
   "$dataOk,1,temperatura, cucina, 23;"    esci e vai avanti
   "$dataNak,1, temperatura, cucina;"       funzione ricorsiva
   ""                                     funzione ricorsiva
   
   RISPOSTA                                            MAXI
   "$dataOk;"                             esci e il MINI controlla che la comunicazione è arivata                                                       
   "dataNak;"                             Richiedo ritrasmissione
 * */
int Radio::gestioneComunicazioneSensoriMini (String stringaRicevuta, int stato) {
  switch (stato) {

  case 0: //invio con controllo della stringa
    {
    bool ok2 = false;
    long WDTTimer;
    Serial.print("Dato da Inviare ====> ");
    Serial.println (sensorData);
    if (generale.getNumberOfElementsOfString(stringaRicevuta) == 3){
      int numeroSensore = generale.getFirstElementOfString(stringaRicevuta, 0).toInt();
      stringaRicevuta = generale.deleteFirstElementOfString(stringaRicevuta, 0);
      int rigaSensore = generale.cercaSensore(String(numeroSensore));
      String stringaOk = "";
      if(sensorType == "temperatura" || sensorType == "pm10"){
        stringaOk = "$dataOk," + String (numeroSensore) + "," + sensorType + "," + sensorPosition + "," + String(sensorData) + "," + String(sensorData1) + ";";
      }
      else{
        stringaOk = "$dataOk," + String (numeroSensore) + "," + sensorType + "," + sensorPosition + "," + String(sensorData) + ";";
      }
      
      String stringaNak = "$dataNak," + String (numeroSensore) + "," + sensorType + "," + sensorPosition + ";";
      
      if (generale.getFirstElementOfString(stringaRicevuta, 0) == sensorType) {
        stringaRicevuta = generale.deleteFirstElementOfString(stringaRicevuta, 0);
        if(stringaRicevuta == sensorPosition) {
          WDTTimer = millis();
          while(( millis () - WDTTimer ) <= WDTMaxTimeRicezione && !ok2){        //invioDati
            ok2 = radio.invio (stringaOk , AirCloudCentraleNode);
          }
          if(ok2){                         //invio avvenuto, aspetto per la conferma del Maxi
            return 3;
          }
          else{                           //invio dell'ok fallito, riprovo con un'invio semplice (case 1)
            return 1;
          }
        }
        else {
          WDTTimer = millis();
          while(( millis () - WDTTimer ) <= WDTMaxTimeRicezione && !ok2){
            radio.invio (stringaNak , AirCloudCentraleNode);                    //stringa ricevuta sbagliata, si aspetta fino alla prossima trasmissione del maxi
          }
          return 4;
        }
      }
      else {
        WDTTimer = millis();
        while(( millis () - WDTTimer ) <= WDTMaxTimeRicezione && !ok2){
          ok2 = radio.invio (stringaNak, AirCloudCentraleNode);                       //stringa ricevuta sbagliata, si aspetta fino alla prossima trasmissione del maxi
        }
        return 4;
      }                                 //ho inviato un nak, in ogni caso aspetto la ritrasmissione del maxi
    }
  }

    
    case 1: //invio semplice (nessuno controllo sulla stringa di entrata)
    {
    bool ok2 = false;
    long WDTTimer = millis();
    while(( millis () - WDTTimer ) <= WDTMaxTimeRicezione && !ok2){        //invioDati
      String stringaOk = "";
      if(sensorType == "temperatura" || sensorType == "pm10"){
        stringaOk = "$dataOk," + String (numeroSensore) + "," + sensorType + "," + sensorPosition + "," + String(sensorData) + "," + String(sensorData1) + ";";
      }
      else{
        stringaOk = "$dataOk," + String (numeroSensore) + "," + sensorType + "," + sensorPosition + "," + String(sensorData) + ";";
      }
      ok2 = radio.invio (stringaOk , AirCloudCentraleNode);
    
    }
    if(ok2){                           //invio avvenuto, aspetto per la conferma del Maxi
      return 3;
    }
    else{                           //invio dell'ok fallito, riprovo con un'invio semplice (case 1)
      return 1;
    }
  }
    case 3: //ricezione della stringa di conferma
    {
    bool ok = 0;
    long tempoPrec = millis();
    while (!ok && (millis() - tempoPrec) < WDTMaxTimeRicezione){
      ok = ricezione ();
    }
    if(ok == 1 && (stringaAttuale.indexOf("$data") == 0)){
      
      stringaAttuale = stringaAttuale.substring(5);
      if(stringaAttuale.indexOf("Ok") == 0) {
        return 4;
      }
      else{
        return 1;
      }
    }
    else {
      return 1;
    }
    break;
    }
  }
}





/**
 
   ESEMPIO COMUNICAZIONE DI INIZALIZZAZIONE
   "$inizializzazione, temperatura, cucina, 01"
   RISPOSTA
   "$inizializzazioneOk;"
   
 * */
void Radio::gestisciInizializzazioneMaxi (String stringaRicevuta) {
  if (generale.getNumberOfElementsOfString(stringaRicevuta) == 3) {       //controllo che i dati inviati sono 3 (posizione, tipo, indirizzo)

    int riga = generale.trovaLaPrimaRigaLibera(0, "sensori");

    sensoriAirCloud[riga][0] = generale.stringaSensoriToEnum(generale.getFirstElementOfString (stringaRicevuta, 0));     //non è sicuramente l'ultimo elemento, in quanto ho gia controllato che gli elementi sono 3
    stringaRicevuta = generale.deleteFirstElementOfString(stringaRicevuta, 0);

    sensoriAirCloud[riga][1] = generale.stringaPosizioneToEnum(generale.getFirstElementOfString (stringaRicevuta, 0));
    stringaRicevuta = generale.deleteFirstElementOfString(stringaRicevuta, 0);

    sensoriAirCloud[riga][2] = generale.getFirstElementOfString(stringaRicevuta, 1).toInt();

    sensoriAirCloud[riga][3] = riga;              //inserimento del numero assegnato al sensore

    /*
    alexa.creazioneArraySensori();
    alexa.creazioneArrayPosizioni();

    alexa.sendDataToDweetio("aggiornamento file per divisione sensori", "", "", "", 0);
    alexa.sendDataToDweetio("aggiornamento file per divisone posizioni", "", "", "", 0);
    alexa.sendDataToDweetio("Aggiornamento pagina sensore singolo", "getData", generale.trasformaEnumTipoInStringa(sensoriAirCloud[riga][0]), generale.trasformaEnumPosizioneInStringa(sensoriAirCloud[riga][1]), 0);
    */
    
    invio("$inzializzazioneOk;", sensoriAirCloud[riga][2]);
  }
}
