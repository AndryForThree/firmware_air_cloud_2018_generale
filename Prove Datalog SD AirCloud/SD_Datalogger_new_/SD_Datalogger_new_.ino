
#include <SPI.h>
#include <SD.h>

const int chipSelect = 4;
int i  = 0;

int year  = 0;
int month = 1;
int day   = 2;
int hour  = 3;
int minu  = 4;
int temp  = 5;
int hum   = 6;
int CO2   = 7;
int CO    = 8;
int PM25  = 9;
int PM10  = 10;


void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }


  Serial.print("Initializing SD card...");

  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    return;
  }
  Serial.println("card initialized.");
}

void loop() {

//creazione stringa di legenda

String infoString = ("year"   "\t"
                     "month"  "\t"
                     "day"    "\t"
                     "hour"   "\t"
                     "minute" "\t"
                     "temp"   "\t"
                     "hum"    "\t"
                     "CO2"    "\t"
                     "PM2.5"  "\t"
                     "PM10"   "\t"
                     "CO"         );

String yearString  = String(year);
String monthString = String(month);
String dayString   = String(day);
String hourString  = String(hour);
String minuString  = String(minu);
String tempString  = String(temp);
String humString  = String(hum);
String CO2String  = String(CO2);
String COString  = String(CO);
String PM25String  = String(PM25);
String PM10String  = String(PM10);

String allString = (yearString  + "\t" + 
                    monthString + "\t" + 
                    dayString   + "\t" + 
                    hourString  + "\t" + 
                    minuString  + "\t" +
                    tempString  + "\t" +
                    humString   + "\t" +
                    CO2String   + "\t" +
                    COString    + "\t" +
                    PM25String  + "\t" +
                    PM10String           );

  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  File dataFile = SD.open("datalog.txt", FILE_WRITE);

  // if the file is available, write to it:
  if (dataFile && i < 60) {

    if(i == 0){
    dataFile.println(infoString); 
    Serial.println(infoString);   
    }
    
    dataFile.println(allString); 
    dataFile.close();
    // print to the serial port too:

    Serial.println(allString);
    i++;
  }
  // if the file isn't open, pop up an error:
  else {
    Serial.println("error opening datalog.txt");
  }
}









