#ifndef INCLUDES_RADIO_AIRCLOUD_H_
#define INCLUDES_RADIO_AIRCLOUD_H_

#include <RF24Network.h>
#include <RF24.h>
#include <SPI.h>


#include "Generale.h"

#include "Board.h"


extern int sensoriAirCloud[15][3];
extern int numeroSensoriAirCloud;
extern String stringaAttuale;

extern class Generale generale;



class Radio {

    public:
        Radio();
        void inizializzazione(const uint16_t);
        bool ricezione();
        bool inviaInizializzazione(const uint16_t, const uint16_t, String, String);
        bool gestioneStringhe(String);
        bool inviaRichiestaDati(int);
        void richiediDati(int);
        bool invio(String, const uint16_t);

    private:
        
        void gestisciInizializzazione(String);
        void gestioneComunicazioneSensori(String);
};

#endif // _INCLUDES_RADIO_AIRCLOUD_H_ 
