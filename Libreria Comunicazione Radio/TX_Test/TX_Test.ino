#include "Radio_AirCloud.h"

const uint16_t this_node = 01;   // Address of our node in Octal format ( 04,031, etc)
const uint16_t master00 = 00;    // Address of the other node in Octal format

/*
int this_node = 01;   // Address of our node in Octal format ( 04,031, etc)
int master00 = 00;    // Address of the other node in Octal format
*/
const String Dato1 = "$Andrea:123:456:256:457;";
const String Dato2 = "$Matteo;";

String sensorType = "umidita";
String sensorPosition = "cucina";
String stringaAttuale;
Radio Radio;


void setup() {
    Serial.begin(9600);
    delay(10);
    Radio.inizializzazione(this_node);
}

void loop() {
    Radio.invio("$inizializzazione, umidita, cucina, 01;", master00);
    delay(200);
    Serial.println ("ciao");
    Radio.invio(Dato2, master00);
    delay(100);
    }
