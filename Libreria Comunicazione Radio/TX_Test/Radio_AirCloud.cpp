#include "Radio_AirCloud.h"

RF24 radioOriginale(2, 15);              // nRF24L01 (CE,CSN)
RF24Network network(radioOriginale);     // Include the radio in the network

//enum dei sensori
enum tipiSensori {
  temperatura = 1,
  umidita = 2,
  CO2 = 3,
  VCO = 4,
  pm10 = 5,
  pm25 = 6,
  metano = 7
};

//enum delle posizioni possibili
enum posizioniSensori {
  salotto = 1,
  cucina = 2,
  bagno = 3,
  cameraDaLetto = 4,
  sgabuzzino = 5,
  garage = 6,
  soffitta = 7,
  cantina = 8
};


Radio::Radio() {

}


void Radio::inizializzazione(const uint16_t this_node) {
  SPI.begin();
  radioOriginale.begin();
  network.begin(90, this_node);               //(channel, node address)
  radioOriginale.setDataRate(RF24_250KBPS);            //oppure setDataRate(RF24_250KBPS); quale conviene?
  radioOriginale.setPALevel(RF24_PA_MAX);          //potenza di funzionamento
}


bool Radio::invio(String stringaDaInviare, const uint16_t nodeToSend) {
  Serial.print("stringaDaInviare: ");
  Serial.print(stringaDaInviare);
  
  network.update();
  int len = stringaDaInviare.length();
  char sendData [50];
  stringaDaInviare.toCharArray(sendData, len);

  RF24NetworkHeader header1(nodeToSend);
  
  bool ok = network.write(header1, &sendData, sizeof(sendData));
  if (ok)
  {
    Serial.println("ok.");
  }
  else
  {
    Serial.println("failed.");
  }
  return ok;
}


bool Radio::ricezione() {
  network.update();
  
  if ( network.available() )                              //da cambiare con un if
  {
    int len = 150;                                          //char receivedData[1];
    char gotmsg[150] = "";

    RF24NetworkHeader header;

    network.read(header, &gotmsg, sizeof(gotmsg)); 
    String Ciao = String (gotmsg);
    stringaAttuale = Ciao;
    Serial.println (stringaAttuale);
    return 1;
  }
  return 0;
}



bool Radio::inviaInizializzazione(const uint16_t nodeToSend, const uint16_t thisNode, String sensorType, String sensorPosition) {
  
}


bool Radio::inviaRichiestaDati(int numeroSensore) {
  uint16_t indirizzoSensore = sensoriAirCloud[numeroSensore][3];
  return invio("richiesta dati", indirizzoSensore);
}


bool Radio::gestioneStringhe(String stringaRicevuta) {
  if (stringaRicevuta.indexOf("inizializzazione") == 1) {                     //controllo se la stringa ricevuta è una stringa di inzializzazione
    gestisciInizializzazione(stringaRicevuta.substring(1));
    return 1;
  }
  else {
    if (stringaRicevuta.indexOf("data") == 1)    {
      gestioneComunicazioneSensori(stringaRicevuta.substring(1));
      return 1;
    }
    else return 0;                                                                //stringa ricevuta non è di inizializ
  }
}

/** 
   ESEMPIO DI COMUNICAZIONE dei sensori
   "$richiestaDati, temperatura, cucina"

   RIPOSTA DEL MINI
   "$dataOk, temperatura, cucina, 23;"
   "$dataNak, temperatura, cucina;"
   
   RISPOSTA
   "$dataOk;"
   "dataNak;"
 * */
void Radio::gestioneComunicazioneSensori(String stringaRicevuta) {
  Serial.println("è arrivata una comunicazione ma non e amìncora implementata");
}




/**
 
   ESEMPIO COMUNICAZIONE DI INIZALIZZAZIONE
   "$inizializzazione, temperatura, cucina, 01"
   RISPOSTA
   "$inizializzazioneOk;"
   
 * */
void Radio::gestisciInizializzazione (String stringaRicevuta) {
  if (generale.getNumberOfElementsOfString(stringaRicevuta) == 3) {       //controllo che i dati inviati sono 3 (posizione, tipo, indirizzo)

    int riga = generale.trovaLaPrimaRigaLibera(0, "sensori");

    sensoriAirCloud[riga][0] = generale.stringaSensoriToEnum(generale.getFirstElementOfString (stringaRicevuta, 0));     //non è sicuramente l'ultimo elemento, in quanto ho gia controllato che gli elementi sono 3
    stringaRicevuta = generale.deleteFirstElementOfString(stringaRicevuta, 0);

    sensoriAirCloud[riga][1] = generale.stringaPosizioneToEnum(generale.getFirstElementOfString (stringaRicevuta, 0));
    stringaRicevuta = generale.deleteFirstElementOfString(stringaRicevuta, 0);

    sensoriAirCloud[riga][2] = generale.getFirstElementOfString(stringaRicevuta, 1).toInt();

    numeroSensoriAirCloud++;

    /*
    alexa.creazioneArraySensori();
    alexa.creazioneArrayPosizioni();

    alexa.sendDataToDweetio("aggiornamento file per divisione sensori", "", "", "", 0);
    alexa.sendDataToDweetio("aggiornamento file per divisone posizioni", "", "", "", 0);
    alexa.sendDataToDweetio("Aggiornamento pagina sensore singolo", "getData", generale.trasformaEnumTipoInStringa(sensoriAirCloud[riga][0]), generale.trasformaEnumPosizioneInStringa(sensoriAirCloud[riga][1]), 0);
    */
    invio("inzializzazioneOk", sensoriAirCloud[riga][2]);
  }
}

void Radio::richiediDati (int rigaSensore) {
  String stringadaInviare = "$data," + generale.trasformaEnumTipoInStringa(sensoriAirCloud[rigaSensore][0]) + "," + generale.trasformaEnumPosizioneInStringa(sensoriAirCloud[rigaSensore][1]) + ";";
  invio (stringadaInviare, generale.intToUnit16_t (sensoriAirCloud[rigaSensore][2]));
}
