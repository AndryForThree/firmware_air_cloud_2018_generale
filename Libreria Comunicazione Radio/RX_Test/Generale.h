#ifndef INCLUDES_GENERALE_H_
#define INCLUDES_GENERALE_H_

#include "Board.h"
#include "Comunicazione_Alexa_AirCloud.h"
#include "Radio_AirCloud.h"

extern int sensoriAirCloud[15][5];
extern int divisionePerTipo [8][5];
extern int divisionePerPosizione [9][10];
extern int numeroSensoriAirCloud;
extern int numeroMassimoSensori;

extern class Radio radio;
extern class Alexa alexa;

class Generale {
  
  public:
  
    Generale();
    
    //trasformazioni Stringhe-Int
    String trasformaEnumPosizioneInStringa(int);
    String trasformaEnumTipoInStringa (int);
    int stringaPosizioneToEnum(String);
    int stringaSensoriToEnum(String);

    //trasformazioni int-uint16_t
    int uint16_tToInt(uint16_t);
    uint16_t intToUnit16_t (int);

    //gestione stringhe
    String getFirstElementOfString(String, bool);
    int getNumberOfElementsOfString(String);
    String deleteFirstElementOfString(String, bool);

    //funzioni per gestire gli array
    int trovaLaPrimaRigaLibera(int, String);
    int trovaLaPrimaColonnaLibera(int, String);
    int cercaSensore(String);

  private:
    
};



#endif // _INCLUDES_RADIO_AIRCLOUD_H_ 
