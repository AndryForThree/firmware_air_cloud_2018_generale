#include "Radio_AirCloud.h"

RF24 radioOriginale(2, 15);               // nRF24L01 (CE,CSN)
RF24Network network(radioOriginale);     // Include the radio in the network


long WDTMaxTime = 2000;
int livello = 0;


//enum dei sensori
enum tipiSensori {
  temperatura = 1,
  umidita = 2,
  CO2 = 3,
  VCO = 4,
  pm10 = 5,
  pm25 = 6,
  metano = 7
};

//enum delle posizioni possibili
enum posizioniSensori {
  salotto = 1,
  cucina = 2,
  bagno = 3,
  cameraDaLetto = 4,
  sgabuzzino = 5,
  garage = 6,
  soffitta = 7,
  cantina = 8
};


Radio::Radio() {

}


void Radio::inizializzazione(const uint16_t this_node) {
  SPI.begin();
  radioOriginale.begin();
  network.begin(90, this_node);               //(channel, node address)
  radioOriginale.setDataRate(RF24_250KBPS);            //oppure setDataRate(RF24_250KBPS); quale conviene?
  radioOriginale.setPALevel(RF24_PA_MAX);          //potenza di funzionamento
}


bool Radio::invio(String stringaDaInviare, const uint16_t nodeToSend) {
  //Serial.print ("Stringa INVIATA: ");
  //Serial.println(stringaDaInviare);
  network.update();
  int len = stringaDaInviare.length();
  char sendData [50];
  stringaDaInviare.toCharArray(sendData, len);

  RF24NetworkHeader header1(nodeToSend);

  bool ok = network.write(header1, &sendData, sizeof(sendData));
  /*if (ok)
  {
    Serial.println("ok.");
  }
  else
  {
    Serial.println("failed.");
  }*/
  return ok;
}


bool Radio::ricezione() {
  network.update();

  if ( network.available() )                              //da cambiare con un if
  {
    int len = 150;                                          //char receivedData[1];
    char gotmsg[150] = "";

    RF24NetworkHeader header;

    network.read(header, &gotmsg, sizeof(gotmsg)); 
    String Ciao = String (gotmsg);
    stringaAttuale = Ciao;
    Serial.print ("Stringa RICEVUTA: ");
    Serial.println (stringaAttuale);
    return 1;
  }
  return 0;
}


/**
 
   ESEMPIO COMUNICAZIONE DI INIZALIZZAZIONE
   "$inizializzazione, temperatura, cucina, 01"
   RISPOSTA
   "$inizializzazioneOk;"
   
 * */
 short Radio::inviaInizializzazione(const uint16_t nodeToSend, const uint16_t thisNode, String sensorType, String sensorPosition, short stato) {

  switch (stato) {
  
  case 0:   //preparazione delle stringhe e invio
  {
  String stringaDaInviare = "$inizializzazione, " + sensorType + "," + sensorPosition + "," + String(nodeToSend) + ";";
  Serial.print ("Questa è la stringa che invio via radio: ");
  Serial.println (stringaDaInviare);
  bool ok = 0;

  //Invio della Stringa
  while (!ok) {
    delay (500);
    Serial.println ("Sto inviando");
    ok = invio (stringaDaInviare, nodeToSend);
  }
  return 1;
  break;
  }
  
  case 1:   //ricezione della RISPOSTA
  {
  bool ok = 0;
  long tempoPrec = millis();
  while (!ok && (millis() - tempoPrec) < WDTMaxTime){
    ok = ricezione ();
  }

  if (ok == 1) {        //ho ricevuto qualcosa
    if (stringaAttuale == "$inizializzazioneOk") {
      Serial.println ("INIZIALIZZAZIONE COMPLETATA");
      return 2;
    }
    else {
      Serial.println ("Ho ricevuto qualcosa di sbagliato");
      return 0;
    }
  }
  else {
    Serial.println ("Non ho ricevuto nulla");
    return 0;
  }
  break;
  }
  }
}


bool Radio::inviaRichiestaDati(int numeroSensore) {
  int rigaSensore = generale.cercaSensore(String(numeroSensore));
  String stringaDaInviare = "$richiestaDati," + String(sensoriAirCloud[rigaSensore][3]) + "," + generale.trasformaEnumTipoInStringa(sensoriAirCloud[rigaSensore][0]) + "," + generale.trasformaEnumPosizioneInStringa(sensoriAirCloud[rigaSensore][1]) + ";"; 
  //Serial.println (stringaDaInviare);
  return invio (stringaDaInviare, sensoriAirCloud[rigaSensore][2]);
}


int Radio::gestioneStringheMaxi(String stringaRicevuta) {
  if (stringaRicevuta.indexOf("inizializzazione") == 1) {                     //controllo se la stringa ricevuta è una stringa di inzializzazione
    Serial.println ("---------------------------------------------------------");
    Serial.println ("INIZIO GESTIONE INIZIALIZZAZIONE");
    gestisciInizializzazioneMaxi (stringaRicevuta.substring(18));
    alexa.creazioneArraySensori();
    alexa.creazioneArrayPosizioni();
    alexa.sendDataToDweetio("aggiornamento file per divisione sensori","","","",15);
    alexa.sendDataToDweetio("aggiornamento file per divisone posizioni", "","","",12);
    Serial.println ("FINE GESTIONE INIZIALIZZAZIONE");
    Serial.println ("---------------------------------------------------------");
    Serial.println ("------------------------------------------------------------------------------------");
      Serial.print ("Dati sensore 1: ");
      Serial.print (sensoriAirCloud[0][0]);
      Serial.print ("\t");
      Serial.print (sensoriAirCloud[0][1]);
      Serial.print ("\t");
      Serial.print (sensoriAirCloud[0][2]);
      Serial.print ("\t");
      Serial.print (sensoriAirCloud[0][3]);
      Serial.print ("\t");
      Serial.print (sensoriAirCloud[0][4]);
      Serial.println("");
      Serial.print("Dati sensore 2: ");
       Serial.print (sensoriAirCloud[1][0]);
      Serial.print ("\t");
      Serial.print (sensoriAirCloud[1][1]);
      Serial.print ("\t");
      Serial.print (sensoriAirCloud[1][2]);
      Serial.print ("\t");
      Serial.print (sensoriAirCloud[1][3]);
      Serial.print ("\t");
      Serial.print (sensoriAirCloud[1][4]);
      Serial.println("");
      Serial.print("Dati sensore 3: ");
      Serial.print (sensoriAirCloud[2][0]);
      Serial.print ("\t");
      Serial.print (sensoriAirCloud[2][1]);
      Serial.print ("\t");
      Serial.print (sensoriAirCloud[2][2]);
      Serial.print ("\t");
      Serial.print (sensoriAirCloud[2][3]);
      Serial.print ("\t");
      Serial.print (sensoriAirCloud[2][4]);
      Serial.println("");
      Serial.print("Dati sensore 4: ");
      Serial.print (sensoriAirCloud[3][0]);
      Serial.print ("\t");
      Serial.print (sensoriAirCloud[3][1]);
      Serial.print ("\t");
      Serial.print (sensoriAirCloud[3][2]);
      Serial.print ("\t");
      Serial.print (sensoriAirCloud[3][3]);
      Serial.print ("\t");
      Serial.println (sensoriAirCloud[3][4]);
      Serial.println ("------------------------------------------------------------------------------------");
    
    stringaAttuale = "";
    return 1;
  }
  else {
    if (stringaRicevuta.indexOf("data") == 1)    {
      gestioneComunicazioneSensoriMaxi (stringaRicevuta.substring(5));
      return 2;
    }
    else return 0;                                                                //stringa ricevuta non è di inizializ
  }
}

bool Radio::gestioneStringheMini(String stringaRicevuta) {
  
  if (stringaRicevuta.indexOf("inizializzazione") == 1) {                     //controllo se la stringa ricevuta è una stringa di inzializzazione
    gestisciInizializzazioneMini(stringaRicevuta.substring(18));
    return 1;
  }
    else return 0;                                                                //stringa ricevuta non è di inizializ
  }


/** 
   ESEMPIO DI COMUNICAZIONE dei sensori                 MAXI
   "$richiestaDati,1,temperatura, cucina"

   RIPOSTA DEL MINI                                     MINI
   "$dataOk,1,temperatura, cucina, 23;"    esci e vai avanti
   "$dataNak,1,temperatura, cucina;"       funzione ricorsiva
   ""                                     funzione ricorsiva
   
   RISPOSTA                                            MAXI
   "$dataOk;"                             esci e il MINI controlla che la comunicazione è arivata                                                       
   "dataNak;"                             Richiedo ritrasmissione
 * */

void Radio::gestioneComunicazioneSensoriMaxi (String stringaRicevuta) {
  if (stringaRicevuta.indexOf("Ok") == 0){        //ho ricevuto una comunicazione di ok
    
    stringaRicevuta = generale.deleteFirstElementOfString(stringaRicevuta, 0);
    String numeroRicevuto = generale.getFirstElementOfString(stringaRicevuta, 0);
    //Serial.println (numeroRicevuto);
    int rigaSensore = generale.cercaSensore(numeroRicevuto);
    /*
    Serial.print ("Riga sensore da stampare: "); 
    Serial.println ( rigaSensore );
    Serial.println ( generale.getFirstElementOfString(stringaRicevuta, 0) );*/
    stringaRicevuta = generale.deleteFirstElementOfString(stringaRicevuta, 0);
    //Serial.println ( generale.trasformaEnumTipoInStringa(sensoriAirCloud[rigaSensore][0]) ); 
    
    if (generale.getFirstElementOfString(stringaRicevuta, 0) == generale.trasformaEnumTipoInStringa(sensoriAirCloud[rigaSensore][0])) {
      stringaRicevuta = generale.deleteFirstElementOfString(stringaRicevuta, 0);
      if (generale.getFirstElementOfString(stringaRicevuta, 0) == generale.trasformaEnumPosizioneInStringa(sensoriAirCloud[rigaSensore][1])) {
        stringaRicevuta = generale.deleteFirstElementOfString(stringaRicevuta, 0);
        int suc = stringaRicevuta.toInt();
        sensoriAirCloud[rigaSensore][4] = suc;
        invio ("$dataOk;", sensoriAirCloud [rigaSensore][2]);
        Serial.print ("DATO RICEVUTO ===> ");
        Serial.print (sensoriAirCloud[rigaSensore][4]);
        Serial.print ("   posizionato alla riga: ");
        Serial.println(rigaSensore);
        alexa.sendDataToDweetio("Aggiornamento pagina sensore singolo", "getData", generale.trasformaEnumTipoInStringa(sensoriAirCloud[rigaSensore][0]), generale.trasformaEnumPosizioneInStringa(sensoriAirCloud[rigaSensore][1]), sensoriAirCloud[rigaSensore][4]);
      }
      else {
        //Serial.println ("Ciao0");
        invio ("$dataNak;", sensoriAirCloud [rigaSensore][2]);
      }
    }
    else {
      //Serial.println ("Ciao1");
      invio ("$dataNak;", sensoriAirCloud [rigaSensore][2]);
    }
  }
  else {
    //Serial.println ("Ciao2");
    stringaRicevuta = generale.deleteFirstElementOfString(stringaRicevuta, 0);
    int rigaSensore = generale.cercaSensore(generale.getFirstElementOfString(stringaRicevuta, 0));
    invio ("$dataNak;", sensoriAirCloud [rigaSensore][2]);
  }
}

void Radio::gestioneComunicazioneSensoriMini (String stringaRicevuta) {
  Serial.println("è arrivata una comunicazione ma non e amìncora implementata");
}


/**
 
   ESEMPIO COMUNICAZIONE DI INIZALIZZAZIONE
   "$inizializzazione, temperatura, cucina, 01"
   RISPOSTA
   "$inizializzazioneOk;"
   
 * */
void Radio::gestisciInizializzazioneMaxi (String stringaRicevuta) {
  if (generale.getNumberOfElementsOfString(stringaRicevuta) == 3) {       //controllo che i dati inviati sono 3 (posizione, tipo, indirizzo)
    numeroSensori++;
    int riga = generale.trovaLaPrimaRigaLibera(0, "sensori");
    
    sensoriAirCloud[riga][0] = generale.stringaSensoriToEnum(generale.getFirstElementOfString (stringaRicevuta, 0));     //non è sicuramente l'ultimo elemento, in quanto ho gia controllato che gli elementi sono 3
    stringaRicevuta = generale.deleteFirstElementOfString(stringaRicevuta, 0);
    
    sensoriAirCloud[riga][1] = generale.stringaPosizioneToEnum(generale.getFirstElementOfString (stringaRicevuta, 0));
    stringaRicevuta = generale.deleteFirstElementOfString(stringaRicevuta, 0);
    
    //indirizzo
    sensoriAirCloud[riga][2] = generale.getFirstElementOfString(stringaRicevuta, 1).toInt();
    
    
    sensoriAirCloud[riga][3] = riga;                                     //inserimento del numero assegnato al sensore
    Serial.print ("Numero assegnato al sensore: ");
    Serial.println (riga);
    
    /*
    alexa.creazioneArraySensori();
    alexa.creazioneArrayPosizioni();

    alexa.sendDataToDweetio("aggiornamento file per divisione sensori", "", "", "", 0);
    alexa.sendDataToDweetio("aggiornamento file per divisone posizioni", "", "", "", 0);
    alexa.sendDataToDweetio("Aggiornamento pagina sensore singolo", "getData", generale.trasformaEnumTipoInStringa(sensoriAirCloud[riga][0]), generale.trasformaEnumPosizioneInStringa(sensoriAirCloud[riga][1]), 0);
    */
    
    delay(5);
    String stringaDaInviare = "$inizializzazioneOk," + String(riga) + ";";
    bool ok = 0;
    while (!ok){
      ok = invio(stringaDaInviare, sensoriAirCloud[riga][2]);
      delay (100);
    }
    
  }
}

void Radio::gestisciInizializzazioneMini (String stringaRicevuta) {
  
}
