#include "Generale.h"

enum tipiSensori{
    temperatura = 1,
    umidita = 2,
    CO2 = 3,
    VCO = 4,
    pm10 = 5,
    pm25 = 6,
    metano = 7
  };

  //enum delle posizioni possibili
   enum posizioniSensori {
    salotto = 1,
    cucina = 2,
    bagno = 3,
    cameraDaLetto = 4,
    sgabuzzino = 5,
    garage = 6,
    soffitta = 7,
    cantina = 8
  };

Alexa alexa;
Radio radio;
Generale generale;

Generale::Generale(){

}

String Generale::trasformaEnumPosizioneInStringa(int posizioneSensore){
  switch (posizioneSensore){

    case 0:
    return "error";
    
    case 1:
    return "salotto";

    case 2:
    return "cucina";
    
    case 3:
    return "bagno";
    
    case 4:
    return "cameraDaLetto";
    
    case 5:
    return "sgabuzzino";
    
    case 6:
    return "garage";
    
    case 7:
    return "soffitta";

    case 8:
    return "cantina";
  }
}

String Generale::trasformaEnumTipoInStringa (int tipoSensore) {
    switch (tipoSensore){
    
    case 0:
    return "error";
    
        case 1:
        return "temperatura";

        case 2:
        return "umidita";
        
        case 3:
        return "CO2";
        
        case 4:
        return "VCO";
        
        case 5:
        return "pm10";
        
        case 6:
        return "pm25";
        
        case 7:
        return "metano";
    }
}

int Generale::stringaPosizioneToEnum(String stringaPosizione){
    if(stringaPosizione == "salotto") return salotto;
    if(stringaPosizione == "cucina") return cucina;
    if(stringaPosizione == "bagno") return bagno;
    if(stringaPosizione == "cameraDaLetto") return cameraDaLetto;
    if(stringaPosizione == "sgabuzzino") return sgabuzzino;
    if(stringaPosizione == "garage") return garage;
    if(stringaPosizione == "soffitta") return soffitta;
    if(stringaPosizione == "cantina") return cantina;
}

int Generale::stringaSensoriToEnum(String stringaSensori){
    if(stringaSensori == "temperatura") return temperatura;
    if(stringaSensori == "umidita") return umidita;
    if(stringaSensori == "CO2") return CO2;
    if(stringaSensori == "VCO") return VCO;
    if(stringaSensori == "pm10") return pm10;
    if(stringaSensori == "pm25") return pm25;
    if(stringaSensori == "metano") return metano;
}

int Generale::getNumberOfElementsOfString(String stringaRicevuta) {
  
    int posizioneVirgola = stringaRicevuta.indexOf(",");
    int numberOfElements = 0;
    
    while (posizioneVirgola != -1){
      
      posizioneVirgola = stringaRicevuta.indexOf(",");
      stringaRicevuta = deleteFirstElementOfString(stringaRicevuta, 0);     
      numberOfElements++;
      
    }
    
    if(stringaRicevuta == ""){
      return 0;
    }
    else {                  //c'è sicuramente un elemento nella stringa
      return numberOfElements;
    }
}

String Generale::getFirstElementOfString(String stringaRicevuta, bool ultimoElemento){
    if (ultimoElemento == 0) {                                                    //non è l'ultimo elemento
        return stringaRicevuta.substring(0, stringaRicevuta.indexOf(","));
    }
    else {                                                                        //è l'ultimo elemento
        return stringaRicevuta;
    }
}

String Generale::deleteFirstElementOfString(String stringaRicevuta, bool ultimoElemento) {
  if(ultimoElemento == 0){
    return stringaRicevuta.substring(stringaRicevuta.indexOf(",") + 1, stringaRicevuta.length());
  }
  else return "";
}

int Generale::trovaLaPrimaColonnaLibera (int filaInCuiCercare, String arrayDoveCercare){
  if(arrayDoveCercare == "tipo"){
    for(int i = 0; i < 5; i++){
        if(divisionePerTipo[filaInCuiCercare][i] == 0) return i;
    }
  }
  if(arrayDoveCercare == "posizione"){
    for(int i = 0; i < 10; i++){
        if(divisionePerPosizione[filaInCuiCercare][i] == 0) return i;
    }
  }

  if(arrayDoveCercare == "sensori"){
    for(int i = 0; i < 15; i++){
        if(sensoriAirCloud [filaInCuiCercare][i] == 0) return i;
    }
  }
}


int Generale::trovaLaPrimaRigaLibera(int colonnaInCuiCercare, String arrayDoveCercare){
  if(arrayDoveCercare == "tipo"){
      for(int i = 0; i < 5; i++){
          if(divisionePerTipo[i][colonnaInCuiCercare] == 0) return i;
      }
    }
    if(arrayDoveCercare == "posizione"){
      for(int i = 0; i < 10; i++){
          if(divisionePerPosizione[i][colonnaInCuiCercare] == 0) return i;
      }
    }

    if(arrayDoveCercare == "sensori"){
      for(int i = 0; i < 15; i++){
          //Serial.print ("ricerca in array: ");
          //Serial.println(sensoriAirCloud[i][colonnaInCuiCercare]);
          if(sensoriAirCloud[i][colonnaInCuiCercare] == 0) return i;
          
      }
    }
}

int Generale::uint16_tToInt(uint16_t numeroIniziale){
  String Stringa = String (numeroIniziale);
  return Stringa.toInt();
}


uint16_t Generale::intToUnit16_t (int intIngresso){
  String stringa = String (intIngresso);
  uint16_t first  = atoi (stringa.substring(1, 3).c_str ());
  return first;
}

int Generale::cercaSensore(String numeroSensoreString) {
  int numeroSensore = numeroSensoreString.toInt();
  for (int i = 0; i < numeroMassimoSensori; i++) {
    /*Serial.print ("numero nell' array: ");
    Serial.println (sensoriAirCloud[i][3]);
    Serial.print ("i: ");
    Serial.println (i);*/
    if (numeroSensore == sensoriAirCloud[i][3]){
      return i;
    }
  }
}
