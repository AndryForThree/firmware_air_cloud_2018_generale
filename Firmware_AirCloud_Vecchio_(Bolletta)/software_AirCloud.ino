#include <DHT.h>
#include <SoftwareSerial.h>
#include <SPI.h>         
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
// Adafruit_PCD8544 display = Adafruit_PCD8544(SCLK, DIN, DC, CS, RST);
Adafruit_PCD8544 display = Adafruit_PCD8544(3, 4, 5, 7, 6);
#include <Wire.h>

#define pwmPin 9    //definire pin per sensore mhz-19
#define DHTPIN 8    //definiamo il pin data 
#define DHTTYPE DHT11  // definiamo il modello del nostro sensore

 DHT dht(DHTPIN, DHTTYPE);

 IPAddress ip(10,150,5,208);
 IPAddress ips(10,150,5,70);


uint8_t picatura[8]  = {B00000001,B00000001,B00000001,B00000001,B00000110,B00000110,B00000110,B00000110};
uint8_t picatura2[8]  = {B10000000,B10000000,B10000000,B10000000,B01100000,B01100000,B01100000,B01100000};
uint8_t picatura3[8]  = {B00011000,B00011000,B00011000,B00011000,B00011000,B00011000,B00000111,B00000111};
uint8_t picatura4[8]  = {B00011000,B00011000,B00011000,B00011000,B00011000,B00011000,B11100000,B11100000};

uint8_t MEC[8]  = {B00000000,B00000000,B00000000,B00011100,B00011110,B00011111,B00001111,B00000111};
uint8_t MEC2[8]  ={B00000000,B00011000,B00111100,B00111100,B00111100,B00111100,B11111111,B11111111};
uint8_t MEC3[8]  ={B00000000,B00000000,B00000000,B00111000,B01111000,B11111000,B11110000,B11100000};
uint8_t MEC4[8]  ={B00000011,B00000011,B00111111,B01111111,B01111111,B00111111,B00000011,B00000011};
uint8_t MEC5[8]  ={B11111111,B11000011,B10000001,B10000001,B10000001,B10000001,B11000011,B11111111};
uint8_t MEC6[8]  ={B11000000,B11000000,B11111100,B11111110,B11111110,B11111100,B11000000,B11000000};
uint8_t MEC7[8]  ={B00000111,B00001111,B00011111,B00011110,B00011100,B00000000,B00000000,B00000000};
uint8_t MEC8[8]  ={B11111111,B11111111,B00111100,B00111100,B00111100,B00111100,B00011000,B00000000};
uint8_t MEC9[8]  ={B11100000,B11110000,B11111000,B01111000,B00111000,B00000000,B00000000,B00000000};



int preheatSec = 3;
int prevVal = LOW;
long th, tl, h, l = 0.0;
long CO2 =1500;
byte mac[] = { 0x90, 0xA2, 0xDA, 0x0F, 0xB2 ,0xB9 };
unsigned int localPort = 8888;      // local port to listen  on

int hu=50;
float te=30.0;
int temp=te;

//char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  //buffer to hold incoming packet,
//char ReplyBuffer[] = "acknowledged";       // a string to send back
char message[21];
String string="";


// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;




void setup() 
{
    Serial.begin(9600);
    Ethernet.begin(mac, ip);
    display.begin();
    DisplayINIT();
     Udp.begin(localPort); 
    dht.begin();
   
    Wire.begin();
    Wire.beginTransmission(0x68);
    Wire.write(0x07); // move pointer to SQW address
    Wire.write(0x10); // sends 0x10 (hex) 00010000 (binary) to control register - turns on square wave
    Wire.endTransmission();
   
    pinMode(pwmPin, INPUT); 
}


void loop()
{
  
    if (preheatSec > 0)
    {
     
      preheatSec--;
     
      if(preheatSec > 0 && preheatSec <10 || preheatSec==0)
      {
         display.setCursor(34,22);
      }
      else
      {
         display.setCursor(22,22);
      }
       
      display.setTextSize(3);
      display.setTextColor(BLACK);
      
      display.print(preheatSec);       
      display.setTextSize(1);
      display.setTextColor(BLACK);
      display.setCursor(10,4);
      display.print("PRE-HEATING:");
      display.display();
      delay(1000);
      display.clearDisplay(); 
    }
    
    else 
    {      
   CO2_READ();
   TH();
   UDPsend();
   stringCreate();
   DISPLAYlcd();
    }
   
}


//-------------------------------------------------


void CO2_READ()
{
    do {
        th = pulseIn(pwmPin, HIGH, 1004000) / 1000.0;
        tl = 1004 - th;
        CO2 = 2000 * (th-2)/(th+tl-4);
    } while (CO2 < 1.0);
    
    if(CO2>1500)
    {
       Serial.println("STATE A MORIIIIIII");
    }
}

//-------------------------------------------------

void TH()
  {
    hu = dht.readHumidity(); // leggiamo il valore per l'umidita'
    te = dht.readTemperature();
    temp=te;
  }

 
//-------------------------------------------------


void UDPsend()
  {
    Udp.beginPacket(ips, 8888);
    stringCreate();
    Udp.write(message);
    Udp.endPacket();
  }


  //-------------------------------------------------


  void stringCreate()
  {
    string = "o;k;";
    string = string + String(te,1);
    string = string + ";";
    string = string + String(hu,DEC);
    string = string + ";";
    string = string + String(CO2,DEC);
    string = string + ";";
    string.toCharArray(message,21);
  }


  //-------------------------------------------------


void DISPLAYlcd()
{
   display.clearDisplay(); 
   display.setTextSize(1);
   display.setTextColor(BLACK);
   display.setCursor(9,2);
   display.print("TEMPERATURE");
   
   display.setTextSize(4);
   display.setTextColor(BLACK);
   display.setCursor(0,18);
   display.print(temp);

 // Serial.println(temp);                //                    stringa aggiunta
   
   display.setCursor(59,18);
   display.print("C");
   
   display.setTextSize(2);
   display.setTextColor(BLACK);
   display.setCursor(47,18);
   display.print("o");
   
   display.display();

   delay(5000);
   display.clearDisplay(); 

   display.setTextSize(4);
   display.setCursor(0,18);
   display.print(hu);
   
 // Serial.println(hu);                //                    stringa aggiunta
   
   display.drawBitmap(70, 27,  picatura, 8, 8, 1);
   display.drawBitmap(78, 27,  picatura2, 8, 8, 1);
   display.drawBitmap(70, 35,  picatura3, 8, 8, 1);
   display.drawBitmap(78, 35,  picatura4, 8, 8, 1);

   display.setTextSize(1);
   display.setTextColor(BLACK);
   display.setCursor(18,2);
   display.print("HUMIDITY");
   
  
   display.setTextSize(3);
   display.setTextColor(BLACK);
   display.setCursor(52,24);
   display.print("%");
   display.display();

   delay (5000);
   display.clearDisplay();  

   display.setTextSize(1);
   display.setCursor(18,2);
   display.setTextColor(BLACK);
   display.print("CO  [ppm]");
   
   display.setTextSize(0);
   display.setCursor(30,5);
   display.setTextColor(BLACK);
   display.print("2");
   
   display.setTextSize(3);
   display.setTextColor(BLACK);
      if(CO2 > 0 && CO2<1000 )
      {
         display.setCursor(16,24);
      }
      else
      {
         display.setCursor(7,24);
      }
  
   display.print(CO2);

  // Serial.println(CO2);                //                    stringa aggiunta
   
   display.display();
   
   delay (4000);
   display.clearDisplay();
  
}



  //-------------------------------------------------

void DisplayINIT()
{
    display.setContrast(50);
    display.clearDisplay();
    display.drawBitmap(30, 15,  MEC, 8, 8, 1);
    display.drawBitmap(38, 15,  MEC2, 8, 8, 1);
    display.drawBitmap(46, 15,  MEC3, 8, 8, 1);
    display.drawBitmap(30, 23,  MEC4, 8, 8, 1);
    display.drawBitmap(38, 23,  MEC5, 8, 8, 1);
    display.drawBitmap(46, 23,  MEC6, 8, 8, 1);
    display.drawBitmap(30, 30,  MEC7, 8, 8, 1);
    display.drawBitmap(38, 30,  MEC8, 8, 8, 1);
    display.drawBitmap(46, 30,  MEC9, 8, 8, 1);
   
    display.setTextSize(1);
    display.setTextColor(BLACK);
    display.setCursor(27,2);
    display.print("I.I.S");
 
    display.setTextSize(1);
    display.setTextColor(BLACK);
    display.setCursor(5,40);
    display.print("VOLTERRA ELIA");
    display.display();

    delay (3000);
    display.clearDisplay();

    display.setTextSize(2);
    display.setTextColor(BLACK);
    display.setCursor(12,8);
    display.print("i");

    display.setTextSize(1);
    display.setTextColor(BLACK);
    display.setCursor(22,17);
    display.print("r");
    
    display.setTextSize(2);
    display.setTextColor(BLACK);
    display.setCursor(28,8);
    display.print("C");

    display.setTextSize(1);
    display.setTextColor(BLACK);
    display.setCursor(39,14);
    display.print("ONTROL");

    display.setTextSize(1);
    display.setTextColor(BLACK);
    display.setCursor(5,35);
    display.print("Air.Q.Control");
    
    display.display();

    delay (5000);
    display.clearDisplay();
    
}

  


  //--------------UTILITY------------------
    /*
  display.setTextSize(1);
  display.setTextColor(BLACK);
  display.setCursor(0,0);
  display.println("tehnic.go.ro");
  display.setCursor(24, 8);
  display.print("& niq_ro");
  display.setCursor(1, 24);
  display.print("ceas-calendar");  
  display.setCursor(0, 32);
  display.print("temp. & umidit");
  display.setCursor(0, 40);
  display.print("versiunea ");
  display.setTextColor(WHITE, BLACK);
  display.print("1.6");
  display.display();
  delay (4000);
  display.clearDisplay(); 
  */

